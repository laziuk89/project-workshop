package pl.zajecia19.clinic;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import pl.zajecia19.clinic.staff.Doctor;

import javax.print.Doc;

import java.util.List;

import static org.junit.Assert.*;

public class ClinicClientTest {

    private static final int EXPECTED_DOCTORS_SIZE_2 = 2;
    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void searchSpeciality() {
        Schedule schedule = new Schedule();
        schedule.addDoctor(new Doctor(Speciality.INTERNISTA, "Krzysztof", "Nowak", "01234567890123"));
        schedule.addDoctor(new Doctor(Speciality.PEDIATRA, "Jan", "Kowalski", "01234567890124"));
        schedule.addDoctor(new Doctor(Speciality.INTERNISTA, "Mateusz", "Spychalski", "01234567890125"));
        ClinicClient clinicClient = new ClinicClient(schedule);
        List<Doctor> foundDoctors = clinicClient.searchSpeciality(Speciality.INTERNISTA);
//        assertNotNull("should give the speciality", foundDoctors );
        assertEquals("wrong list size", EXPECTED_DOCTORS_SIZE_2, foundDoctors.size() );
    }
}