package pl.zajecia19.clinic;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import pl.zajecia19.clinic.staff.Doctor;

import java.util.List;
import java.util.Random;

import static org.junit.Assert.*;

public class ScheduleUtilsTest {

    private Schedule schedule;

    @Before
    public void setUp() {
        schedule = new Schedule();

        Doctor janNowakDoctor = new Doctor(Speciality.INTERNISTA, "Jan", "Nowak", "01234567891");
        Doctor krzysztofJudym = new Doctor(Speciality.GINEKOLOG, "Krzysztof", "Judym", "01234567892");
        Doctor stefanNiesiolowski = new Doctor(Speciality.INTERNISTA, "Stefan", "Niesiolowski", "01234567893");
        Doctor krzysztofKrawczyk = new Doctor(Speciality.PEDIATRA, "Krzysztof", "Krawczyk", "01234567894");
        Doctor piotrNowak = new Doctor(Speciality.GINEKOLOG, "Piotr", "Nowak", "01234567895");
        Doctor lordVoldemort = new Doctor(Speciality.GINEKOLOG, "Lord", "Voldemort", "01234567896");
        Doctor harryPotter = new Doctor(Speciality.INTERNISTA, "Harry", "Potter", "01234567897");
        Doctor bradPitt = new Doctor(Speciality.PEDIATRA, "Brad", "Pitt", "01234567898");

        schedule.addDoctor(janNowakDoctor);
        schedule.addDoctor(krzysztofJudym);
        schedule.addDoctor(stefanNiesiolowski);
        schedule.addDoctor(krzysztofKrawczyk);
        schedule.addDoctor(piotrNowak);
        schedule.addDoctor(lordVoldemort);
        schedule.addDoctor(harryPotter);
        schedule.addDoctor(bradPitt);
    }

    @After
    public void tearDown(){
        schedule = null;
    }

    @Test
    public void fillSchedule() {

        System.out.println("testing fillSchedule");
        List<Visit> visits = ScheduleUtils.fillSchedule(schedule);
        System.out.println(visits);

        assertNotNull("visit list should not be null", visits);
    }

    @Test
    public void fillSchedule2() {

        System.out.println("testing fillSchedule 2");
        List<Visit> visits = ScheduleUtils.fillSchedule(schedule);
        int size = visits.size();

        assertEquals("list size not equal", ScheduleUtils.ONE_MONTH, size);

    }

    @Test
    public void fillSchedule3() {
        Schedule schedule = new Schedule();
        System.out.println("testing fillSchedule3");

//        assertEquals("");
    }
    @Test
    public void testRandom(){
        Random random = new Random();
        System.out.println(random.nextInt(10));



    }
}