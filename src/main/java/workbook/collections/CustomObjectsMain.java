package workbook.collections;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;

public class CustomObjectsMain {
    public static void main(String[] args) {

        Person p1 = new Person(1, "Adam");
        Person p2 = new Person(3, "Krzysiek");
        Person p3 = new Person(2, "Tomasz");
        Person p4 = new Person(1, "Adam");


        ///////////////////////hashset

        HashSet<Person> personHashSet = new HashSet<>();

        personHashSet.add(p1);
        personHashSet.add(p2);
        personHashSet.add(p3);
        personHashSet.add(p4);

        /////////////////////hashmap

        HashMap<Person, String> personHashMap = new HashMap<>();
        personHashMap.put(p1, "aaaa");
        personHashMap.put(p2, "bbbb");
        personHashMap.put(p3, "cccc");
        personHashMap.put(p4, "ddddd");

        for (Person key : personHashMap.keySet()) {
            System.out.println(key + " " + personHashMap.get(key));
        }

        System.out.println();

        Iterator<Person> personIterator = personHashSet.iterator();
        while (personIterator.hasNext()) {
            System.out.println(personIterator.next());
        }
        System.out.println();
    }
}

class Person {

    private int id;
    private String name;

    public Person(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Pet{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return id == person.id &&
                Objects.equals(name, person.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
