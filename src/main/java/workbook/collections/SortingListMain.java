package workbook.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SortingListMain {
    public static void main(String[] args) {

        List<String> animals = new ArrayList<>();

        animals.add("cat");
        animals.add("penguin");
        animals.add("horse");
        animals.add("whale");
        animals.add("dog");


        for (String animal : animals) {
            System.out.println(animal);
        }
        System.out.println("\n");
        Collections.sort(animals, new StringComparisonAlphabet());
        for (String animal : animals) {
            System.out.println(animal);
        }

        System.out.println();
        Collections.sort(animals, new StringComparisonLength());

        for (String animal : animals) {
            System.out.println(animal);
        }

        ArrayList<Integer> listOfIntegers = new ArrayList<>();

        listOfIntegers.add(10);
        listOfIntegers.add(3);
        listOfIntegers.add(2);
        listOfIntegers.add(6);
        listOfIntegers.add(5);

        Collections.sort(listOfIntegers);
        System.out.println();
        for (Integer listofInts : listOfIntegers) {
            System.out.println(listofInts);
        }

        List<Computer> computers = new ArrayList<>();

        computers.add(new Computer(3000, "Asus"));
        computers.add(new Computer(2000, "Acer"));
        computers.add(new Computer(2500, "apple"));
        computers.add(new Computer(1500, "HP"));


        for (Computer computer : computers) {
            System.out.println(computer);
        }
        System.out.println();

        Collections.sort(computers, new Comparator<Computer>() {
            @Override
            public int compare(Computer o1, Computer o2) {
                if (o1.getPrice() > o2.getPrice()) {
                    return 1;
                } else if (o1.getPrice() < o2.getPrice()) {
                    return -1;
                }
                return 0;
            }
        });
        for (Computer computer : computers) {
            System.out.println(computer);
        }
    }

    ;
}


class StringComparisonLength implements Comparator<String> {


    @Override
    public int compare(String o1, String o2) {
        int len1 = o1.length();
        int len2 = o2.length();
        if (len1 > len2) {
            return 1;
        } else if (len1 < len2) {
            return -1;
        }
        return 0;
    }
}

class StringComparisonAlphabet implements Comparator<String> {

    public int compare(String o1, String o2) {
        return -o1.compareTo(o2);
    }
}

class Computer {

    private int price;
    private String brand;

    public Computer(int price, String brand) {
        this.price = price;
        this.brand = brand;
    }

    public int getPrice() {
        return price;
    }

    public String getBrand() {
        return brand;
    }

    @Override
    public String toString() {
        return "Computer{" +
                "price=" + price +
                ", brand='" + brand + '\'' +
                '}';
    }
}