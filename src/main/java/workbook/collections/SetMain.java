package workbook.collections;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class SetMain {
    public static void main(String[] args) {

        HashSet<String> hashSetSet1 = new HashSet<>();

        hashSetSet1.add("cat");
        hashSetSet1.add("dog");
        hashSetSet1.add("snake");
        hashSetSet1.add("horse");

        for (String set : hashSetSet1) {
            System.out.println(set);
        }

        TreeSet<String> treeSetSet2 = new TreeSet<>();

        treeSetSet2.add("bee");
        treeSetSet2.add("whale");
        treeSetSet2.add("cat");
        treeSetSet2.add("horse");

        System.out.println();
        for (String set : treeSetSet2){
            System.out.println(set);
        }


        TreeSet<String> commonParts = new TreeSet<>(treeSetSet2);
        commonParts.retainAll(hashSetSet1);
        System.out.println("\n" + commonParts);

        TreeSet<String> difference = new TreeSet<>(treeSetSet2);
        difference.removeAll(hashSetSet1);
        System.out.println(difference);

        if (treeSetSet2.contains("cat")){
            System.out.println("contains cat");
        }
    }
}
