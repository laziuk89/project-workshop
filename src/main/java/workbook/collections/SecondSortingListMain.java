package workbook.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SecondSortingListMain {
    public static void main(String[] args) {

        List<Restaurant> restaurants = new ArrayList<>();

        restaurants.add(new Restaurant(5, "Swietokrzyska"));
        restaurants.add(new Restaurant(2, "Pańska"));
        restaurants.add(new Restaurant(3, "chmielna"));
        restaurants.add(new Restaurant(1, "Belgradzka"));
        restaurants.add(new Restaurant(4, "Nowy Swiat"));

        for (Restaurant restaurant : restaurants) {
            System.out.println(restaurant);
        }
        System.out.println();
        Collections.sort(restaurants, new Comparator<Restaurant>() {

            public int compare(Restaurant o1, Restaurant o2) {
                if (o1.getId() > o2.getId()) {
                    return 1;
                } else if (o1.getId() < o2.getId()) {
                    return -1;
                }
                return 0;
            }
        });

        for (Restaurant restaurant : restaurants) {
            System.out.println(restaurant);
        }

        System.out.println();
        Collections.sort(restaurants, new Comparator<Restaurant>() {

            public int compare(Restaurant o1, Restaurant o2) {
                return o1.getName().compareToIgnoreCase(o2.getName());
            }
        });
        for (Restaurant restaurant1 : restaurants) {
            System.out.println(restaurant1);
        }

    }
}
class Restaurant {

    private int id;
    private String name;

    public Restaurant(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Restaurant{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
