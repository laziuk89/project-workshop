package workbook.collections;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;

public class LinkedMain {
    public static void main(String[] args) {
        LinkedList<String> animals = new LinkedList<>();

        animals.add("cat");
        animals.add("dog");
        animals.add("horse");

        System.out.println(animals);
        for (String listOfAnimals : animals){
            System.out.println(listOfAnimals);
        }

        animals.add(2, "ELEPHANT");
        System.out.println();
        for (String listOfAnimals : animals) {
            System.out.println(listOfAnimals);
        }

        Iterator iterator = animals.listIterator();
        while (iterator.hasNext()) {
            System.out.println( ((ListIterator) iterator).nextIndex() + " " + iterator.next());
        }

        System.out.println();
        while (((ListIterator) iterator).hasPrevious()){
            System.out.println(((ListIterator) iterator).previousIndex() + " " + ((ListIterator) iterator).previous());
        }


    }
}
