package workbook.collections;

import java.util.HashMap;
import java.util.Map;

public class AppHashMap {
    public static void main(String[] args) {

        Map<Integer, String> hashMap = new HashMap<>();

        hashMap.put(2, "two");
        hashMap.put(1, "one");
        hashMap.put(5, "five");
        hashMap.put(8, "eight");
        hashMap.put(7, "seven");


        System.out.println(hashMap.get(1));


        for (Map.Entry<Integer, String> map : hashMap.entrySet()) {

            int key = map.getKey();
            String value = map.getValue();

            System.out.println(key + ": " + value);
        }

        for (Map.Entry<Integer, String> map : hashMap.entrySet()) {
            int key = map.getKey();
            String value = map.getValue();

            System.out.println(key + " " + value);
        }

        System.out.println();

        for (Integer mapIterator : hashMap.keySet()) {
            String value = hashMap.get(mapIterator);
            System.out.println(mapIterator + ":" + value);
        }

        for (Map.Entry<Integer, String> mapIterator : hashMap.entrySet()) {
            int key = mapIterator.getKey();
            String value = mapIterator.getValue();
            System.out.println(key + " " + value);
        }
        for (Integer mapIterator : hashMap.keySet()) {
            String value = hashMap.get(mapIterator);
            System.out.println(mapIterator + value);
        }
    }
}
