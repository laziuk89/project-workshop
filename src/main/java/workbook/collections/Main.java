package workbook.collections;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

public class Main {
    public static void main(String[] args) {

        TreeMap treeMap = new TreeMap();
        HashMap hashMap = new HashMap();
        LinkedHashMap linkedHashMap = new LinkedHashMap();

        testMap(treeMap);
        System.out.println();
        testMap(hashMap);
        System.out.println();
        testMap(linkedHashMap);

    }

public static void testMap(Map<Integer, String> map){
        map.put(4, "four");
        map.put(2, "two");
        map.put(10, "ten");
        map.put(7, "seven");

        for (Map.Entry<Integer, String> showMap : map.entrySet()){
            int key = showMap.getKey();
            String value = showMap.getValue();
            System.out.println(key + " " + value);

        }
}
}
