package workbook.lambdas.first;

import workbook.lambdas.first.Calculator;

public class Main {
    public static void main(String[] args) {
        Calculator minus = (int x, int y) -> x-y;


        System.out.println(minus.calculate(3,9));
    }
}
