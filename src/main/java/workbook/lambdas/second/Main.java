package workbook.lambdas.second;

import java.util.Comparator;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        Comparator<String> comparator = new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareTo(o2);
            }
        };
        int x = comparator.compare("hello", "world");
        System.out.println(x);



        //ctrl +alt + v/c
        Comparator<String> lambdaComparator = (String obj1, String obj2) -> {
            return obj1.compareTo(obj2);
        };
        int y = lambdaComparator.compare("hello", "world");

        System.out.println(y);

        Myfunction myfunction = (String firstText, String secondText) -> System.out.println(firstText + " " + secondText);
        myfunction.function("apply", "some method");

        Myfunction secondMyFunction = (String thirdText, String fourthText) -> System.out.println(thirdText + " " + fourthText);
        secondMyFunction.function("apply", "another method");


        Myfunction.speak("Maciek", 30);

        Myfunction siemens = new Phone();
        siemens.function("smartphone", " functioning");
        siemens.text("texting");


    }

    static class Phone implements Myfunction {

        @Override
        public void function(String text1, String text2) {
            System.out.println("functioning from the static method" + text1);
        }

        @Override
        public void text(String text) {
            System.out.println("texting from the static method " + text);
        }
    }
}

