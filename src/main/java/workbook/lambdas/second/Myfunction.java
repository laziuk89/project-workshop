package workbook.lambdas.second;

public interface Myfunction {
    void function(String text1, String text2);

    static void speak(String name, int age){

        if (age>25){
            System.out.println(name + " " + age);
        }
    };

    default void text(String text){
        System.out.println(text);
    };
}
