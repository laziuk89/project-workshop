package workbook.lambdas.thirdVariableCapture;

public interface MyInterface {
    void speak(String text);
}
