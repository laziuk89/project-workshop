package workbook.lambdas.thirdVariableCapture;

public class Main {
    static int data = 40;
    private String fourthVariableMember = "||member variable ";

    public void doIt() {


        MyInterface interfaceFromMain = (text) -> {
            System.out.println(text + fourthVariableMember);
        };
        interfaceFromMain.speak("text from the doIt method ");
        fourthVariableMember = "||changed value of the fourthVariableMember ";
        interfaceFromMain.speak("text from the doIt method ");

    }

    public static void main(String[] args) {
        final String textFromMain = "hello from Pet ";

        MyInterface myInterface = (text) -> System.out.println(text + textFromMain + data);
        myInterface.speak("parameter speaking + ");

        Main.data = 50;
        myInterface.speak("parameter speaking + ");

//        MEMBER VARIABLE MUSI BYC FINAL LUB EFFECTIVELY FINAL(NIE MOZE BYC ZMIENIONE W POZNIEJSZYM KODZIE JESLI WCZESNIEJ
//        UZYWAMY GO W LAMBDZIE
//        textFromMain = "lkgnel";


        System.out.println();
        Main obj1 = new Main();
        obj1.doIt();
    }
}

