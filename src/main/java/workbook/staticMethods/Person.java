package workbook.staticMethods;

public class Person {
    private int age;
    private String name;

    public static int count;
    private int id;

    public Person() {
        count++;
    }

    @Override
    public String toString() {
        return "Pet{" +
                "age=" + age +
                ", name='" + name + '\'' +
                '}';
    }
}
