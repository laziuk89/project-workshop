package workbook.recursion;

public class Main {
    public static void main(String[] args) {


        System.out.println(getFactorial(7));
    }

    public static int getFactorial(int n){

        if(n<0){
            return -1;
        }else if (n<2) {
            return 1;
        }else return (n*getFactorial(n-1));
        }
}
