package workbook.generics.generics.linkedList;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Main {
    public static void main(String[] args) {


        ArrayList<Integer> arrayList = new ArrayList();
        LinkedList<Integer> linkedList = new LinkedList();

        timeTaken("ArrayList", arrayList);
        timeTaken("LinkedList", linkedList);

    }

    public static void timeTaken(String type,List<Integer> list) {
        for (int i = 0; i < 1E5; i++) {
            list.add(i);
        }

        long start = System.currentTimeMillis();

//        for (int i = 0; i < 1E5; i++) {
//            list.add(i);
//        }

        for (int i = 0; i<1E5; i++){
            list.add(0,i);
        }
        long end = System.currentTimeMillis();


        System.out.println((end-start) + "ms " + type);
    }
}
