package workbook.serialization.computer;

import java.io.Serializable;

public class Computer implements Serializable {
    String brand;
    int ramMemory;

    public Computer(String brand, int ramMemory) {
        this.brand = brand;
        this.ramMemory = ramMemory;
    }

    @Override
    public String toString() {
        return "Computer{" +
                "brand='" + brand + '\'' +
                ", ramMemory=" + ramMemory +
                '}';
    }
}
