package workbook.serialization.computer;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

public class ReadingObject {
    public static void main(String[] args) {

        System.out.println("Reading objects...");
        try {
            FileInputStream fis = new FileInputStream("serializing arrays.txt");
            ObjectInputStream ois = new ObjectInputStream(fis);

            Computer[] computers = (Computer[]) ois.readObject();
            ArrayList<Computer> computersList = (ArrayList<Computer>) ois.readObject();
            for (Computer computer : computers) {
                System.out.println(computer);
            }

            System.out.println();
            for (Computer computer : computersList) {
                System.out.println(computer);
            }
            ois.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
