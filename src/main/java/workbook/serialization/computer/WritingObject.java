package workbook.serialization.computer;


import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;

public class WritingObject {
    public static void main(String[] args) {

        Computer[] computers = {new Computer("Samsung", 350), new Computer("Acer", 600),new Computer("Dell", 1500)};
        ArrayList<Computer> computersList = new ArrayList<>(Arrays.asList(computers));

        System.out.println("Writing Objects...");
        {
            try {
                FileOutputStream fs = new FileOutputStream("serializing arrays.txt");
                ObjectOutputStream ous = new ObjectOutputStream(fs);

                ous.writeObject(computers);
                ous.writeObject(computersList);

                ous.close();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

}
