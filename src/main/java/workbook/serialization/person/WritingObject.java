package workbook.serialization.person;

import workbook.serialization.person.Person;

import java.io.*;

public class WritingObject {
    public static void main(String[] args) {


        Person adam = new Person(30, "Adam");
        Person tomasz = new Person(20, "Tomasz");

        try (FileOutputStream fos = new FileOutputStream("showobjects.bin")) {
            ObjectOutputStream ous = new ObjectOutputStream(fos);

            ous.writeObject(adam);
            ous.writeObject(tomasz);

            ous.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}