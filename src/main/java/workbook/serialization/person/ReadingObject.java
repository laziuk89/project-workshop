package workbook.serialization.person;

import workbook.serialization.person.Person;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

public class ReadingObject {
    public static void main(String[] args) {

   try(FileInputStream fis = new FileInputStream("showobjects.bin")){
       ObjectInputStream ois = new ObjectInputStream(fis);

       Person person1 = (Person) ois.readObject();
       Person person2 = (Person) ois.readObject();

       System.out.println(person1);
       System.out.println(person2);
       ois.close();

    } catch (FileNotFoundException e) {
       e.printStackTrace();
   } catch (IOException e) {
       e.printStackTrace();
   } catch (ClassNotFoundException e) {
       e.printStackTrace();
   }
    }
}
