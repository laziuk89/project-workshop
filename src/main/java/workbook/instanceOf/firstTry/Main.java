package workbook.instanceOf.firstTry;

public class Main {
    public static void main(String[] args) {

        Person[] person = new Person[5];

        person[0] = new Employee("Jan", "Kowalski", 5000);
        person[1] = new Student("Robert", "Nowak", "English philology");

        for (int i = 0; i < person.length; i++) {
            if (person[i] instanceof Person){

                System.out.println();
                person[i].description();
            }else break;
        }
    }
}
