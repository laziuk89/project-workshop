package workbook.instanceOf.firstTry;

public abstract class Person {

    protected String name;
    protected String lastname;


    public Person(String name, String lastname) {
        this.name = name;
        this.lastname = lastname;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", lastname='" + lastname + '\'' +
                '}';
    }

    public void description() {
        System.out.println("imie: " + name);
        System.out.println("nazwisko: " + lastname);
    }
}

class Employee extends Person {

    private int salary;

    public Employee(String name, String lastname, int salary) {
        super(name, lastname);
        this.salary = salary;
    }

    @Override
    public void description() {
        System.out.println("imie: " + name);
        System.out.println("nazwisko: " + lastname);
        System.out.println("wynagrodzenie: " + salary);
    }
}

class Student extends Person {
    String faculty;

    public Student(String name, String lastname, String faculty) {
        super(name, lastname);
        this.faculty = faculty;
    }

    @Override
    public void description() {
        System.out.println("imie: " + name);
        System.out.println("nazwisko: " + lastname);
        System.out.println("kierunek: " + faculty);
    }
}