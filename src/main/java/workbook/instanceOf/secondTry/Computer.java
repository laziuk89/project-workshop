package workbook.instanceOf.secondTry;

public abstract class Computer {

    protected String brand;
    protected int cost;

    public Computer(String brand, int cost) {
        this.brand = brand;
        this.cost = cost;
    }

    @Override
    public String toString() {
        return "Computer{" +
                "brand='" + brand + '\'' +
                ", cost=" + cost +
                '}';
    }

    public void intendedUse(Computer computer) {
        System.out.println(computer + " has can be use in many ways");
    }
}

class Laptop extends Computer {
    protected boolean mobile;

    public Laptop(String brand, int cost, boolean mobile) {
        super(brand, cost);
        this.mobile = mobile;
    }

    @Override
    public void intendedUse(Computer computer) {
        System.out.println(computer + " can be moved around");
    }
}
class Pc extends Computer{

    int quantity;
    public Pc(String brand, int cost, int quantity) {
        super(brand, cost);
        this.quantity=quantity;
    }

    @Override
    public void intendedUse(Computer computer) {
        System.out.println(computer + " can't be really moved around the house");
    }
}
