package workbook.instanceOf.secondTry;


public class Main {
    public static void main(String[] args) {

        Computer[] computers = new Computer[5];
        computers[0] = new Laptop("Samsung", 1500, true);
        computers[1] = new Pc("Asus", 3000, 1);

        for (int i = 0; i < computers.length; i++) {
            if (computers[i] instanceof  Computer){
                computers[i].intendedUse(computers[i]);
            }else break;
        }
    }
}