package workbook.random;

public class Animal {
    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void showName(String name) {
        System.out.println("this animal is: " + name);
    }

    public void feed() {
        System.out.println("feed the pet: ");
    }
}
