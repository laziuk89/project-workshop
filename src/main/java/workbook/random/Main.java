package workbook.random;

public class Main {
    public static void main(String[] args) {

        Pet pet1 = new Pet();
        pet1.name = "Pchelka";

        Animal animal1 = new Animal();
        animal1.name = "Cat";

        pet1.showName(pet1.getName());
        animal1.showName(animal1.getName());
        System.out.println();
        //////////////////////////////////

        Animal animal2 = new Pet();
        animal2.showName("Dog");


        animal2.feed();
        System.out.println();

        doShow(animal2);
        doShow(pet1);
        doShow(animal1);
    }
    public static void doShow(Animal animal){
    animal.showName("this pet/animal is called: " + animal.getName() + " " + animal.getClass());
    }
}
