package workbook.random;

public class Pet extends Animal {
    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void showName(String name) {
        System.out.println("this pet's name: " + name);
    }

}
