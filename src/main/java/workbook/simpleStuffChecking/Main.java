package workbook.simpleStuffChecking;

public class Main {
    public static void main(String[] args) {

        sendSalary(true, 300, 2000);
        sendSalary(false);
    }

    public static String sendSalary(boolean employed) {
        if (employed == false) {
            String info = "jesteś bezrobotny, nie masz wynagrodzenia";
            System.out.println(info);
            return info;
        }
        return String.valueOf(-1);
    }

    private static int sendSalary(boolean employed, int bonus, int basePay) {
        if (employed) {
            int fullSalary = bonus + basePay;
            System.out.println("twoje wynagrodzenie to: " + fullSalary);
            return fullSalary;
        }
        return -1;
    }
}