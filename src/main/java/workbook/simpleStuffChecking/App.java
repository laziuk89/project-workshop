package workbook.simpleStuffChecking;

public class App {
    public static void main(String[] args) {

        int calculate = calculateHighScoreTable(1500);
        displayHighScorePosition("Adam", calculate);

        calculate = calculateHighScoreTable(900);
        displayHighScorePosition("John", calculate);


        calculate = calculateHighScoreTable(400);
        displayHighScorePosition("Thomas", calculate);


        calculate = calculateHighScoreTable(50);
        displayHighScorePosition("Deen", calculate);


    }

    public static String displayHighScorePosition(String playerName, int positionHighScoreTable) {

        String player = playerName + " managed to get to into position: " + positionHighScoreTable + " on the high score table";
        System.out.println(player);
        return player;
    }

    public static Integer calculateHighScoreTable(int playerScore) {
        if (playerScore >= 1000) {
            return 1;

        } else if (playerScore >= 500 && playerScore <= 1000) {
            return 2;
        } else if (playerScore >= 100 && playerScore <= 5000) {
            return 3;
        } else return 4;

    }
}
