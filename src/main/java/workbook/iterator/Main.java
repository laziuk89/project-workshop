package workbook.iterator;

import pl.zajecia21.generics.Pair;

import java.util.*;

public class Main {
    public static void main(String[] args) {

        List<String> animals = new ArrayList<>();

        animals.add("cat");
        animals.add("dog");
        animals.add("bee");
        animals.add("elephant");
        animals.add("crocodile");


        List<Integer> listOfValues = new ArrayList<>();
        listOfValues.add(2);
        listOfValues.add(4);
        listOfValues.add(3);
        listOfValues.add(10);

        Set<String> setOfStrings = new HashSet<>();

        setOfStrings.add("pierwszy String");
        setOfStrings.add("piąty String");
        setOfStrings.add("drugi String");
        setOfStrings.add("dziesiąty String");

        Map<Integer, String> mapRandom = new HashMap<>();
        mapRandom.put(2, "druga mapka");
        mapRandom.put(1, "pierwsza mapka");
        mapRandom.put(5, "piąta mapka");
        mapRandom.put(10, "dziesiąta mapka");


        for (String showList : animals) {
            System.out.println(showList);
        }

        Iterator iterator = animals.listIterator();

        System.out.println("\n======iterator w przód======\n");
        while (iterator.hasNext()) {
            System.out.println(((ListIterator) iterator).nextIndex() + ": " + iterator.next());
        }

        System.out.println("\n=====iterator w tył=====\n");
        while (((ListIterator) iterator).hasPrevious()) {
            System.out.println(((ListIterator) iterator).previousIndex() + ": " + ((ListIterator) iterator).previous());
        }

        System.out.println("\n===== iterator of integers");

        Iterator iteratorListOfValues = listOfValues.listIterator();

        while (iteratorListOfValues.hasNext()) {
            System.out.println(((ListIterator) iteratorListOfValues).nextIndex() + ": " + iteratorListOfValues.next());
        }
        System.out.println("\n===== iterator of integers w tył");
        while (((ListIterator) iteratorListOfValues).hasPrevious()) {
            System.out.println(((ListIterator) iteratorListOfValues).previousIndex() + ": " + ((ListIterator) iteratorListOfValues).previous());
        }

        System.out.println("\n====Set====");
        Iterator iteratorOfSet = setOfStrings.iterator();
        while (iteratorOfSet.hasNext()) {
            System.out.println(iteratorOfSet.next());
        }

        System.out.println("\n=====Mapka======");

        Iterator<Integer> iteratorOfMap = mapRandom.keySet().iterator();
        while (iteratorOfMap.hasNext()) {
            Integer key = iteratorOfMap.next();
            System.out.println(key + ": " + mapRandom.get(key));
        }

        for (Map.Entry<Integer, String> mapIterating : mapRandom.entrySet()) {
            Integer key = mapIterating.getKey();
            String value = mapIterating.getValue();
            System.out.println(key + ": " + value);
        }

        for (Map.Entry<Integer, String> mapIterator : mapRandom.entrySet()) {
            int key = mapIterator.getKey();
            String value = mapIterator.getValue();
            System.out.println(key + ": " + value);

        }

        Iterator<Integer> mapIterator = mapRandom.keySet().iterator();
        while (mapIterator.hasNext()) {
            Integer key = mapIterator.next();
            System.out.println(mapRandom.get(key));
        }

        System.out.println("//////////");
        List<String> someAnimals = new ArrayList<>();

        someAnimals.add("cat");
        someAnimals.add("dog");
        someAnimals.add("penguin");
        someAnimals.add("horse");
        someAnimals.add("bird");

        for (String listSomeAnimals : someAnimals) {
            System.out.println(listSomeAnimals);
        }
        Iterator iteratorSomeAnimals = someAnimals.listIterator();

        while (iteratorSomeAnimals.hasNext()) {
            System.out.println(((ListIterator) iteratorSomeAnimals).nextIndex() + ": " + iteratorSomeAnimals.next());
        }
        System.out.println("///////////////");
        while (((ListIterator) iteratorSomeAnimals).hasPrevious()) {
            System.out.println(((ListIterator) iteratorSomeAnimals).previousIndex() + ": " + ((ListIterator) iteratorSomeAnimals).previous());
        }

        Map<Integer, String> mapOfAnimals = new HashMap<>();
        mapOfAnimals.put(1, "cat");
        mapOfAnimals.put(2, "dog");
        mapOfAnimals.put(3, "horse");
        mapOfAnimals.put(4, "whale");
        mapOfAnimals.put(5, "bird");
        mapOfAnimals.put(6, "dolphin");

        System.out.println("///////////map");
        for (Map.Entry<Integer, String> iteratingMapOfAnimals : mapOfAnimals.entrySet()) {
            Integer key = iteratingMapOfAnimals.getKey();
            String value = iteratingMapOfAnimals.getValue();
            System.out.println(key + ": " + value);
        }

        Iterator<Integer> iteratorMapOfAnimals = mapOfAnimals.keySet().iterator();
        while (iteratorMapOfAnimals.hasNext()) {
            Integer key = iteratorMapOfAnimals.next();
            System.out.println(key + ": " + mapOfAnimals.get(key));
        }


        Map<Integer, String> professions = new HashMap<>();

        professions.put(1, "fotballer");
        professions.put(2, "postman");
        professions.put(3, "nurse");
        professions.put(4, "translator");
        professions.put(5, "bricklayer");

        System.out.println("/////////map2");


        for (Map.Entry<Integer, String> iteratingProf : professions.entrySet()) {
            int key = iteratingProf.getKey();
            String value = iteratingProf.getValue();
            System.out.println(key + ": " + value);
        }
        System.out.println("////map iterator");
        Iterator<Integer> iteratingProf = professions.keySet().iterator();
        while (iteratingProf.hasNext()) {
            Integer key = iteratingProf.next();
            System.out.println(key + ": " + professions.get(key));
        }
//        Iterator<Integer> mapIterator = mapRandom.keySet().iterator();
//        while (mapIterator.hasNext()) {
//            Integer key = mapIterator.next();
//            System.out.println(  mapRandom.get(key));

    }
}
