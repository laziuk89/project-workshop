package workbook.bufferedReader;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        BufferedReader br=null;
        String line = null;

        try {
            br= new BufferedReader(new FileReader("C:\\Users\\xxx\\Desktop\\pliki\\" + scanner.next()));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        
        while (true){
            try {
                if (!((line = br.readLine())!=null)) break;
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println(line);
        }

    }
}
