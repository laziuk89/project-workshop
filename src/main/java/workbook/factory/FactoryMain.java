package workbook.factory;

import java.util.Scanner;

public class FactoryMain {
    public static void main(String[] args) {


    Scanner sc = new Scanner(System.in);
    String line = sc.nextLine();
        System.out.println(line );
    FactoryMain factoryMain = new FactoryMain();
//    Fighter style = factoryMain.getFighterInfo(line);
//    String className = factoryMain.getFighterInfo(line).getClass().getName();
//        System.out.println(style);
//        System.out.println(className);
    }

    Fighter fighter;
    public Fighter getFighterInfo(String string) {

        if (string.equalsIgnoreCase("kickboxing")) {
            fighter = new Kickboxing();
        } else if (string.equalsIgnoreCase("mma")) {
            fighter = new Mma();
        } else if (string.equalsIgnoreCase("box")) {
            fighter = new Box();
        } else
            fighter = new Unreckognized();
        return fighter;
    }
}
