package workbook.factory;

public interface Fighter {

    String fightingStyle();
}
