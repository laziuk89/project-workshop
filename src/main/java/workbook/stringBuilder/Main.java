package workbook.stringBuilder;

public class Main {
    public static void main(String[] args) {

        StringBuilder sb = new StringBuilder();
        sb.append(3).append(" to ").append(" mój ").append("wiek");

        System.out.println(sb);
//////////////////////////////////////////////////////////////////////////

        Person person1 = new Person("Maciek", 30);
//        System.out.println(person1);
        System.out.println(person1.toString());

        Monitor monitor1= new Monitor("Asus", 50);
        System.out.println(monitor1.toString());

    }
}
class Person{

    String name;
    int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(age).append(" ").append(name);
        return stringBuilder.toString();
    }
}
class Monitor{
 String brand;
 int width;

    public Monitor(String brand, int width) {
        this.brand = brand;
        this.width = width;
    }

    @Override
    public String toString() {
        return String.format("%s has got %d", brand, width);
    }
}