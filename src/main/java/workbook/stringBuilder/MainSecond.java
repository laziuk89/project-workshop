package workbook.stringBuilder;

public class MainSecond {
    public static void main(String[] args) {

        Friend friend1 = new Friend("maciek");
        System.out.println(friend1);
    }
}
class Friend{

    String name;

    public Friend(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Friend{" +
                "name='" + name + '\'' +
                '}';
    }
}
