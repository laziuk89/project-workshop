package workbook.recap.equal;

public class Main {
    public static void main(String[] args) {

        Person adam = new Person("Adam", 30);
        Object tomasz = new Person("Tomasz", 25);

        Person tomaszX = new Person("Tomasz", 25);

        System.out.println();
    if (tomasz.equals(tomaszX)){
            System.out.println("są sobie równe");
        }

    }
}
class Person{

    String name;
    int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    public boolean equals(Object ob){
        if(ob ==null){
            return false;
        }
       Person sent = (Person) ob;

        return  this.name== sent.name && this.age == sent.age;
    }
}