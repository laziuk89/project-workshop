package workbook.recap.polimorphism;

public class Main {

    public static void main(String[] args) {

        Student diligent = new DiligentStudent();
        diligent.setAttitude("diligent");
        diligent.attitude();
        Student diligent2 = new Student("David", "diligent");

        Student[] students = new Student[3];

        students[0] = new Student("Jan", "diligent");
        students[1] = new Student("Adam", "indifferent");
        students[2] = new Student("Tom", "lazy");


        for (int x = 0; x <students.length ; x++) {
            students[x].attitude();
        }
    }
}
