package workbook.recap.polimorphism;

public class Student {

    private String name;
    private String attitude;

    public void attitude(){
        System.out.println(" has the following attitude towards the classes " + attitude);
    }

    public Student() {
    }

    public Student(String name, String attitude) {
        this.name = name;
        this.attitude = attitude;
    }

    public String getAttitude() {
        return attitude;
    }

    public void setAttitude(String attitude) {
        this.attitude = attitude;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", attitude='" + attitude + '\'' +
                '}';
    }
}
