package workbook.recap.mapa;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Map<Integer, String> mapaLudzi = new HashMap<>();

        mapaLudzi.put(1, "John");
        mapaLudzi.put(2, "Chris");
        mapaLudzi.put(3, "Tom");
        mapaLudzi.put(4, "Michael");

        for (Map.Entry<Integer,String> mapka: mapaLudzi.entrySet()) {
           int key = mapka.getKey();
           String value= mapka.getValue();
            System.out.println(key + " " + value);
        }
        System.out.println(mapaLudzi.size());
        System.out.println(mapaLudzi.isEmpty());
        System.out.println(mapaLudzi.values());
        System.out.println(mapaLudzi.keySet());
        System.out.println(mapaLudzi);
    }
}
