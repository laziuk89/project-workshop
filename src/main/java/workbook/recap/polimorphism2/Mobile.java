package workbook.recap.polimorphism2;

public class Mobile extends Camera {


    @Override
    public void functionality(Camera camera) {
        System.out.println("calling, texting: \n" + camera.getClass());
    }
}
