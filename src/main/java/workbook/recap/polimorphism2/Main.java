package workbook.recap.polimorphism2;

public class Main {
    public static void main(String[] args) {

        Camera camera = new Camera();
        camera.functionality(camera);
        System.out.println();

        Mobile mobile = new Mobile();
        mobile.functionality(mobile);
        System.out.println();

        Camera camera2 = new Mobile();
        camera2.functionality(camera2);




    }
}
