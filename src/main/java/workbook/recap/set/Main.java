package workbook.recap.set;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class Main {
    public static void main(String[] args) {

//
//        Set<Integer> setInt = new HashSet<>();
//
//        setInt.add(2);
//        setInt.add(5);
//        setInt.add(8);
//        setInt.add(20);
//
//        for (Integer numbers : setInt) {
//
//            System.out.println(numbers);
//
//        }
//        Set<String> setStr = new HashSet<>();
//
//        setStr.add("adam");
//        setStr.add("pawel");
//        setStr.add("damian");
//        setStr.add("jan");
//
//        for (String values : setStr) {
//            System.out.println(values);
//        }


        Set<Integer> lotto = new HashSet<>();
        Random r = new Random();
        while (lotto.size() < 6) {
            int x = r.nextInt(36) + 1;
            System.out.println("wylosowano: " + x);
            lotto.add(x);
        }
        for (Integer numbers : lotto) {
            System.out.println(numbers);
        }

        System.out.println();
        System.out.println("OTHER LOTTO");
        Set<Integer> otherLotto = new HashSet<>();
        while (otherLotto.size() < 10) {
            int check = r.nextInt(100) + 1;
            System.out.println("wylosowano: " + check);
            otherLotto.add(check);

        }
        for (Integer iteracjaPoLotto : otherLotto) {
            System.out.println(iteracjaPoLotto);
        }

        System.out.println("Last random");
        Random random = new Random();
        Set<Integer> myLotto = new HashSet<>();
        while (myLotto.size() < 10) {
            int randomInt = random.nextInt(100) + 1;
            System.out.println(randomInt);
            myLotto.add(randomInt);
        }
        Random nextLottoRandom = new Random();
        Set<Integer> nextLotto = new HashSet<>();
        while (nextLotto.size() < 5) {
            int drawLots = nextLottoRandom.nextInt(40) + 1;
            System.out.println("wylosowano: " + drawLots);
            nextLotto.add(drawLots);

        }
        nextLotto.add(30);
        System.out.println(nextLotto.contains(30));

    }
}
