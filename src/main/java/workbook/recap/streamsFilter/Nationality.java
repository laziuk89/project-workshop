package workbook.recap.streamsFilter;

public enum Nationality {
    POLISH, ENGLISH, GERMAN, DUTCH;
}
