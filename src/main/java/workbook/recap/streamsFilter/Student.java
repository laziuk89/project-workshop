package workbook.recap.streamsFilter;

public class Student {
    private int age;
    private Nationality nationality;
    private boolean promotedToNextClass;
    private Faculty faculty;

    public Student(int age, Nationality nationality, boolean promotedToNextClass, Faculty faculty) {
        this.age = age;
        this.nationality = nationality;
        this.promotedToNextClass = promotedToNextClass;
        this.faculty = faculty;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Nationality getNationality() {
        return nationality;
    }

    public void setNationality(Nationality nationality) {
        this.nationality = nationality;
    }

    public boolean isPromotedToNextClass() {
        return promotedToNextClass;
    }

    public void setPromotedToNextClass(boolean promotedToNextClass) {
        this.promotedToNextClass = promotedToNextClass;
    }

    public Faculty getFaculty() {
        return faculty;
    }

    public void setFaculty(Faculty faculty) {
        this.faculty = faculty;
    }

    @Override
    public String toString() {
        return "Student{" +
                "age=" + age +
                ", nationality=" + nationality +
                ", promotedToNextClass=" + promotedToNextClass +
                ", faculty=" + faculty +
                '}';
    }
}
