package workbook.recap.streamsFilter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {

        List<Student> students = Arrays.asList(

            new Student(30, Nationality.POLISH, true, Faculty.ARCHITECTURE),
            new Student(20, Nationality.ENGLISH, false, Faculty.MATHS),
            new Student(24, Nationality.DUTCH, true, Faculty.LITERATURE),
            new Student(35, Nationality.DUTCH, false, Faculty.LITERATURE)
        );

        students.stream()
                .filter(student -> student.getAge() > 24)
                .filter(student -> student.isPromotedToNextClass()==false)
                .map(student -> student.getAge() + " " + student.getFaculty() + " " + student.getNationality() + " " + student.isPromotedToNextClass())
                .forEach(System.out::println);

    Predicate<Student> studentPredicate = student -> student.getNationality().equals(Nationality.ENGLISH);

    List<Student> secondListStudents = new ArrayList<>();
    secondListStudents = students.stream()
            .filter(studentPredicate)
            .collect(Collectors.toList());

        System.out.println();
        System.out.println(secondListStudents);

        for (Student list:secondListStudents) {
            System.out.println(list);
        }
    }
}
