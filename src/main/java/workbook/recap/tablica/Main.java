package workbook.recap.tablica;

public class Main {
    public static void main(String[] args) {
        int x = 0;

        int value[] = new int[5];
        value[0] = 10;
        value[1] = 20;
        value[2] = 30;
        value[3] = 40;
        value[4] = 50;

        for (int i = 0; i < value.length; i++) {
            System.out.println(i + ": " + value[i]);
        }

        int differentValues[] = {100, 50, 200, 30};
        for (int i = 0; i < differentValues.length; i++) {
            System.out.println(differentValues[i]);
        }
        System.out.println("iterowanie za pomoca Do While");
        do {
            System.out.println(x + ": " + differentValues[x]);
            x++;
        } while (x < differentValues.length);

        char letters[] = {'d', 'g', 'h'};
        for (int i = 0; i < letters.length; i++) {
            System.out.println(letters[i]);
        }


        while (x < letters.length) {
            System.out.println(x + " " + letters[x]);
            x++;
        }


    }
}
