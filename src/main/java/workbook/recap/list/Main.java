package workbook.recap.list;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        List<String> people = new ArrayList<>();
        people.add("Adam");
        people.add("Pawel");
        people.add("Maciek");
        people.add("Stefan");

        System.out.println("iteracja po foreach");
        for (String ludzie : people) {
            System.out.println(ludzie);
        }
        System.out.println();
        System.out.println("iteracja po for");
        for (int i = 0; i < people.size(); i++) {
            System.out.println(people.get(i));
        }


        System.out.println(people.size());
        people.add("tomek");
        System.out.println(people.size());

        people.add(1, "Krzyś");
        System.out.println(people);
        System.out.println(people.contains("Maciek"));
        System.out.println(people.hashCode());
    }
}
