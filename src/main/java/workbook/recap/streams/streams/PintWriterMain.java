package workbook.recap.streams.streams;

import java.io.*;

public class PintWriterMain {
    public static void main(String[] args) {

        try {
            PrintWriter printWriter = new PrintWriter(new FileWriter("PrintWriter.txt"), true);
            printWriter.println(20);
            printWriter.println(12456);
            printWriter.println("ala ma kota");
            printWriter.close();

            BufferedReader bufferedReader = new BufferedReader(new FileReader("PrintWriter.txt"));

            PrintWriter secondWriter = new PrintWriter(new FileWriter("bazowy.txt"));

            String tresc;
            while ((tresc = bufferedReader.readLine()) != null) {
                secondWriter.write(tresc);
                secondWriter.println();
            }

            bufferedReader.close();
            secondWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
