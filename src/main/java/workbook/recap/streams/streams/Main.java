package workbook.recap.streams.streams;

import java.io.*;

public class Main {
    public static void main(String[] args) throws IOException {

        File plik = new File("plik.txt");
        BufferedWriter fileWriter = new BufferedWriter(new FileWriter(plik));


        fileWriter.write("ala ma kota");

        fileWriter.newLine();
        fileWriter.newLine();
        fileWriter.write("druga linijka");
        fileWriter.close();

        Main.checkingMethod("kdsk", 222, "welnw", 22.9980, 9999);

    }

    static void checkingMethod(Object... a) {
        for (int i = 0; i < a.length; i++) {
            System.out.println(a[i]);
        }
    }
}
