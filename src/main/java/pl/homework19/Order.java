package pl.homework19;

public class Order {

    private DishType dishType;
    private TakeAway takeAway;
    private String cashier;
    private int quantity;
    private double price;
    private String currency = "€";
    private String priceWithCurrency;

    public Order(DishType dishType, TakeAway takeAway, String cashier, int quantity, double price) {
        this.dishType = dishType;
        this.takeAway = takeAway;
        this.cashier = cashier;
        this.quantity = quantity;
        this.price=price;
        this.priceWithCurrency = price + " " + currency;
    }

    public DishType getDishType() {
        return dishType;
    }

    public void setDishType(DishType dishType) {
        this.dishType = dishType;
    }

    public TakeAway getTakeAway() {
        return takeAway;
    }

    public void setTakeAway(TakeAway takeAway) {
        this.takeAway = takeAway;
    }

    public String getOrder() {
        return cashier;
    }

    public void setOrder(String order) {
        this.cashier = order;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "Order{" +
                "dishType=" + dishType +
                ", takeAway=" + takeAway +
                ", cashier='" + cashier + '\'' +
                ", quantity=" + quantity +
                ", price=" + price +
                ", priceWithCurrency=" + priceWithCurrency +
                '}';
    }
}
