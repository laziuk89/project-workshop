package pl.homework19;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Waitress {

    private Order order;
    private String waitressName;
    private LocalDateTime orderTime;
    private String orderTimeString;
    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd - HH:MM");

    public Waitress(Order order, String waitressName, LocalDateTime orderTime) {
        this.order = order;
        this.waitressName = waitressName;
        this.orderTime = orderTime;
        this.orderTimeString = dateTimeFormatter.format(orderTime);
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public String getWaitressName() {
        return waitressName;
    }

    public void setWaitressName(String waitressName) {
        this.waitressName = waitressName;
    }

    public LocalDateTime getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(LocalDateTime orderTime) {
        this.orderTime = orderTime;
    }

    @Override
    public String toString() {
        return "Waitress{" +
                "order=" + order +
                ", waitressName='" + waitressName + '\'' +
                ", orderTime=" + orderTime +
                ", orderTimeString=" + orderTimeString +
                '}';
    }
}
