package pl.homework19;


import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {


        List<Waitress> lists = new ArrayList<>();

        Order orderMoktow = new Order(DishType.PIZZA, TakeAway.NO, "Barbara",  1, 44.49);
        Order orderWilanow = new Order(DishType.STEAK, TakeAway.YES, "Stanislaw",  2, 65.99);

        LocalDateTime now = LocalDateTime.now();

        Waitress waitress1 = new Waitress(orderMoktow, "Jadwiga", LocalDateTime.now());
        Waitress waitress2 = new Waitress(orderWilanow, "Marta", LocalDateTime.now());

        lists.add(waitress1);
        lists.add(waitress2);

        for (Waitress waitress : lists){
            System.out.println(waitress);
        }
    }
}
