package pl.zajecia2;

public class PizzaMain {
    public static void main(String[] args) {
        //Pizza-typ zmiennej
        //margerita - nazwa zmiennej
        // = - przypisanie wartości
        //new - tworzenie nowego obiektu
        //Pizza() - wywołanie konstruktora

        Pizza margerita = new Pizza(); //deklaracja zmiennej z przypisaniem wartości

        //inicjalizowanie obiektu przez metody Set
        margerita.setName("dagrasso");
        margerita.setSize(40);

        //tożsame; toString jest niejawnie wywoływane dla obiektu
        System.out.println(margerita);
        System.out.println(margerita.toString());

        //deklaracja zmiennej
        Pizza hawajska;
        //przypisanie wartości do zmiennej
        hawajska = new Pizza(20, "telepizza");//inicjalizowanie obiektu przez konstruktor
        System.out.println(hawajska);

        Pizza roma = new Pizza("pizza hut");
        System.out.println(roma.toString());

        Pizza funghi = new Pizza();

        System.out.println(funghi);
    }
}
