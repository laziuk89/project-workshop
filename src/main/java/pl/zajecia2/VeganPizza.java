package pl.zajecia2;

public class VeganPizza extends Pizza {
    private boolean meat;

    public VeganPizza() {

    }

    public VeganPizza(int size, String name, boolean meat) {
        super(size, name);
        this.meat = meat;
        //this.size=size;

    }

    public boolean isMeat() {
        return meat;
    }

    public void setMeat(boolean meat) {
        this.meat = meat;
    }

    @Override
    public String toString() {
        return "VeganPizza{" +
                "meat=" + meat +
                "} " + super.toString();
    }


}
