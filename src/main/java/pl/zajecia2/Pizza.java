package pl.zajecia2;

public class Pizza {
    private int size;   //pole/atrybut/zmienna w klasie
    private String name;

    public Pizza() {

    }

    public Pizza(String name){

        this.name=name;
    }

    public Pizza(int size, String name) {
        this(name);
        this.size = size;
     //   this.name = name;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "size=" + size +
                ", name='" + name + '\'' +
                '}';
    }

}
