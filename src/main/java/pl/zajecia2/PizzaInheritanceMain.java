package pl.zajecia2;

public class PizzaInheritanceMain {
    public static void main(String[] args) {

        VeganPizza veganPizza = new VeganPizza();
        veganPizza.setName("vegan");
        veganPizza.setMeat(true);
        System.out.println(veganPizza);

        Pizza pizza = new Pizza(90, "pieczarkowa");
       // pizza.set

        VeganPizza rukola = new VeganPizza(23, "rukola", false);

        System.out.println(rukola);
    }
}
