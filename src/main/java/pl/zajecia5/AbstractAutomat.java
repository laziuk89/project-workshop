package pl.zajecia5;

public abstract class AbstractAutomat {

    //metodę abstrakcyjną mogę zrobić tylko w abstrakcyjnej klasie
    public abstract void automat();

    public void  execute() {
        System.out.println("executing from Abstract");
    }
}
