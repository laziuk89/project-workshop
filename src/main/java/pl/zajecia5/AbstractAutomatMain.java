package pl.zajecia5;

public class AbstractAutomatMain {
    public static void main(String[] args) {

        // nie można utworzyć obiektu klasy abstrakcyjnej
        //AbstractAutomat abstractAutomat1 = new AbstractAutomat();

        AbstractAutomat countingAutomat = new AbstractAutomat()
        {
            private int x;

            @Override
            public void automat() {

            }
        };
        countingAutomat.execute();

        //AbstractAutomat - typ obiektu
        //ca - nazwa zmiennej
        //new CountingAutomat() - typ referencji

        AbstractAutomat ca = new CountingAutomat();
        //typ referencji determinuje to która metoda ma zostać wykonana, czy z nadklasy czy nadpisana(@Override)
        ca.execute();

        //typ obiektu determinuje o tym jakie metody możemy wywołać
        //ca.count();

        CountingAutomat counting1 = new CountingAutomat();

        counting1.execute();
        counting1.count();


    }

    public void method() {
        int y = 0;
        System.out.println(y);
    }

}
