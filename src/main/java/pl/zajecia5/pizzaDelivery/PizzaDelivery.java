package pl.zajecia5.pizzaDelivery;

public abstract class PizzaDelivery {
    public void deliver() {
        System.out.println("delivering");
    }

    public abstract void collectCash();
    /*{
        System.out.println("collecting cash");
    }*/
}
