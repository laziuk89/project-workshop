package pl.zajecia5.pizzaDelivery;

public class PizzaDeliveryMain {
    public static void main(String[] args) {

        PizzaDelivery jacekPizzaDelivery = new JacekPizzaDelivery();
        PizzaDelivery maciekPizzaDelivery = new MaciekPizzaDelivery();

        jacekPizzaDelivery.deliver();
        jacekPizzaDelivery.collectCash();

        maciekPizzaDelivery.deliver();
        maciekPizzaDelivery.collectCash();



    }
}
