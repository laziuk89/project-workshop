package pl.homework2;

public class ExClientsMain {
    public static void main(String[] args) {


        ExClient exClient1 = new ExClient("German", 18);
        ExClient exClient2 = new ExClient("Italian", 25);

        exClient1.setDescription(55);
        exClient1.setName("Hans");

        exClient2.setDescription(24);
        exClient2.setName("Bonzo");
        System.out.println(exClient1);
        System.out.println(exClient2);
    }
}