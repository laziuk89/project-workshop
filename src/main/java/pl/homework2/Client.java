package pl.homework2;

public class Client {

    private String name;
    private int description;
    private String country;

    public static int count = 0;

    public int id;


    public Client() {
//        count++;
//      id = count;


    }


    public Client(String country) {
        this();
        this.country = country;
    }

    public Client(String name, int description, String country) {
        this.name = name;
    }
    public static void speak(){
        count++;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDescription() {
        return description;
    }

    public void setDescription(int description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Client{" +
                "name='" + name + '\'' +
                ", description=" + description +
                ", country='" + country + '\'' +
                ", count=" + count +
                ", id=" + id +
                '}';
    }
}





