package pl.homework2;

public class Main {
    public static void main(String[] args) {
        Client.speak();
        Client client1 = new Client();

        client1.setDescription(59);
        client1.setName("Maciek");
        System.out.println(client1.toString() );

        Client.speak();
        Client client2 = new Client();

        client2.setDescription(48);
        client2.setName("Pawel");

        System.out.println(client2.toString() );

        Client.speak();
        Client client3 = new Client("Poland");

        client3.setDescription(89);
        client3.setName("Tomasz");

        System.out.println(client3.toString());

        int id = 0;

        Client client4 = new Client();
        client4.setId(id++);
        System.out.println(client4);

        Client client5 = new Client();
        client5.setId(id++);
        System.out.println(client5);
    }
}
