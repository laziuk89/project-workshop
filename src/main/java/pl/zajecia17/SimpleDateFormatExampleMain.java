package pl.zajecia17;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class SimpleDateFormatExampleMain {
    public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance();
        System.out.println(calendar);
        System.out.println(calendar.getTime());

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
        String formattedDate = sdf.format(calendar.getTime());//służy do formatowania
        System.out.println(formattedDate);

        try {
          //  Date date = sdf.parse("1984.01.01");
            Date date = sdf.parse("1984.01.01");
            System.out.println(date);
        } catch (ParseException exception) {
            exception.printStackTrace();
        }
    }
}
