package pl.zajecia17;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;

public class LocalDateExampleMain {
    public static void main(String[] args) {

        LocalDate localDate = LocalDate.now();

        System.out.println(localDate);
        LocalDateTime localDateTime = LocalDateTime.now();
        System.out.println(localDateTime);

        System.out.println(Calendar.getInstance().getTime());

//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:SS");
//        System.out.println(sdf.format(localDateTime.get));

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy.MM.dd HH:SS");
        System.out.println(dateTimeFormatter.format(localDateTime));

        LocalDateTime yesterday = LocalDateTime.of(2019,1, 29, 0, 0);
        LocalDateTime today = LocalDateTime.now();

        int comparedDates = yesterday.compareTo(today);
        System.out.println(comparedDates);
    }
}
