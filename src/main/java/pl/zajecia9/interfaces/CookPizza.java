package pl.zajecia9.interfaces;

public interface CookPizza {
    void prepareIngredients();
    void putIngredients();
    void bake();
}
