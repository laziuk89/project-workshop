package pl.zajecia9.interfaces;

public class TmobileAgreement implements Agreement {
    @Override
    public boolean sign(String name) {
        System.out.println("Tmobile podpisywanie umowy przez: " + name);
        if (name != null) return true;

        return false;
    }
}
