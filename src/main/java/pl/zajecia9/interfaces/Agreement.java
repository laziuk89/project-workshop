package pl.zajecia9.interfaces;

public interface Agreement {
    boolean sign(String name);

    default void revoke(String name){
        System.out.println("revoking agreement");
    }
}

