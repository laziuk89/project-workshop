package pl.zajecia9.interfaces;

import java.util.ArrayList;
import java.util.List;

public class InterfacesMain {
    public static void main(String[] args) {

        List<Agreement> lista = new ArrayList<>();
        lista.add(new TmobileAgreement());

        OrangeAgreement orangeAgreement = new OrangeAgreement();
        lista.add(orangeAgreement);

        String name="Tomek";

        for (Agreement agreement : lista){
            agreement.sign(name);

        }
    }
}
