package pl.zajecia9.interfaces;

public class OrangeAgreement extends Shape implements Agreement, CookPizza{

    @Override
    public boolean sign(String name) {
        System.out.println("Orange podpisywanie umowy przez: " + name);
        if (name != null && name.length() > 3) return true;

        return false;
    }

    @Override
    public void prepareIngredients() {

    }

    @Override
    public void putIngredients() {

    }

    @Override
    public void bake() {

    }

    @Override
    public void draw() {

    }
}
