package pl.zajecia9.loops;

public class WhileMain {
    public static void main(String[] args) {

        int status = 10;
        while (status > -1) {
            System.out.println("Loop while");
            System.out.println(status);
            status--;
        }

        int x = 2;
        do {
            System.out.println("Loop do while");
            System.out.println(x);
            x--;
        } while (x > 2);
    }
}
