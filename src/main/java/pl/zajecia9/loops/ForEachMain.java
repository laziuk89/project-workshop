package pl.zajecia9.loops;

import java.util.ArrayList;
import java.util.List;

public class ForEachMain {
    public static void main(String[] args) {

        List<Case> lista = new ArrayList<>();
        lista.add(new Case(100));

        Case case1 = new Case(200);
        lista.add(case1);

        int i=lista.size();
        System.out.println(i);

        //pętla for each
        //Case c - deklaracja zmiennej po której iterujemy
        //: lista - zmienna po której iterujemy

        for (Case c : lista) {
            System.out.println(c);
        }
        List<String> stringList = new ArrayList<>();
        stringList.add("telefon");
        stringList.add("monitor");

        for (int j = 0; j <stringList.size() ; j++) {
            System.out.println(stringList);
        }
//        efekt jak w for each poniżej
//        for (int j = 0; j <stringList.size() ; j++) {
//            System.out.println(stringList.get(j));
//        }

        for (String s : stringList){
            System.out.println(s);
        }
    }

    static class Case{
        private int price;

        public Case(int price) {
            this.price = price;
        }

        @Override
        public String toString() {
            return "Case{" +
                    "price=" + price +
                    '}';
        }
    }

}
