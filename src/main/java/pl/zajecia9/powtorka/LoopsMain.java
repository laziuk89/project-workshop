package pl.zajecia9.powtorka;

import java.util.Scanner;

public class LoopsMain {
    public static void main(String[] args) {

        System.out.println("oczekuję na wciśnięcie klawisza z klawiatury");
        Scanner scanner = new Scanner(System.in);
        String line;
        while (!(line = scanner.nextLine()).equals("q")) {
            System.out.println("wczytuję kolejne znaki");
            System.out.println(line);
        }
//        String s = scanner.nextLine();
//
//        System.out.println(s);
//        boolean status = (line = scanner.nextLine()).equals("q");
        line = null;
        System.out.println("machine waiting for the command");
        do {
            System.out.println("waiting for the proper command");
            System.out.println(line);
        } while (!(line = scanner.nextLine()).equalsIgnoreCase("Z"));// ignoruje wielkość litery(case)

    }
}