package pl.zajecia9.powtorka;

public interface Display {

    void paint();

    default void show(){
        System.out.println("showing on display");
    }

}
