package pl.zajecia9.powtorka;

public class IndoorDisplay implements Display{

    @Override
    public void paint() {
        System.out.println("IndoorDisplay painting");
    }
    @Override
    public void show(){
        System.out.println("IndoorDisplay showing");
    }

    @Override
    public String toString() {
        return "IndoorDisplay{}";
    }
}
