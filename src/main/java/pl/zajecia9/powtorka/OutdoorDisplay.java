package pl.zajecia9.powtorka;

public class OutdoorDisplay implements Display {

    @Override
    public void paint(){
        System.out.println("OutdoorDisplay painting");
    }

    @Override
    public String toString() {
        return "OutdoorDisplay{}";
    }
}
