package pl.zajecia9;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListMain {
    public static void main(String[] args) {

        List<String> lista = new ArrayList<>();
        lista.add("hello world");
        lista.add("priwiet ");

        System.out.println(lista);
        lista.remove(1);
        System.out.println(lista);

        List<Integer> integers = new ArrayList<>();
        integers.add(5);

        for (int i=0; i<100;i++){
            integers.add(i);
        }
        System.out.println(integers);

        Integer[] tablicaIntegerow = new Integer[10];
        for (int i = 0; i < 10; i++) {

            tablicaIntegerow[i]=i+11;

        }
        System.out.println(Arrays.asList(tablicaIntegerow));

    }
}
