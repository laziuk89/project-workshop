package pl.zajecia26.database;

import pl.zajecia26.database.model.Book;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BooksDao {
    public static void main(String[] args) {

        List<Book> books = database();
        System.out.println(books);

    }

    public static List<Book> database() {
        List<Book> books = new ArrayList<>();

        String url = "jdbc:postgresql://localhost:5432/postgres";
        String sql = "SELECT * FROM BOOKS";
        try (Connection connection = DriverManager.getConnection(url, "postgres", "postgres");
             PreparedStatement preparedStatement = connection.prepareStatement(sql);
             ResultSet resultSet = preparedStatement.executeQuery()) {

            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                int pages = resultSet.getInt("pages");
                String covers = resultSet.getString("covers");
                String isbn = resultSet.getString("isbn");

                Book book = new Book(id, name, pages, covers, isbn);
//                System.out.println(book);
                books.add(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return books;
    }
}
