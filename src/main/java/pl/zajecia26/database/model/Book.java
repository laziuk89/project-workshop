package pl.zajecia26.database.model;

public class Book {

    //alt+shift+insert - zaznaczanie kolumnowe
    private int id;
    private String name;
    private int pages;
    private String covers;
    private String isbn;

    public Book(int id, String name, int pages, String covers, String isbn) {
        this.id = id;
        this.name = name;
        this.pages = pages;
        this.covers = covers;
        this.isbn = isbn;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public String getCovers() {
        return covers;
    }

    public void setCovers(String covers) {
        this.covers = covers;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", pages=" + pages +
                ", covers='" + covers + '\'' +
                ", isbn='" + isbn + '\'' +
                '}';
    }
}
