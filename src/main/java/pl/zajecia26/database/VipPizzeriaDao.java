package pl.zajecia26.database;

import java.sql.*;

public class VipPizzeriaDao {
    public static void main(String[] args) {

        database();

    }

    public static void database() {
        String url = "jdbc:postgresql://localhost:5432/postgres";

//try-with-resource
        try (Connection connection = DriverManager.getConnection(url, "postgres", "postgres");
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT * FROM public.\"PIZZAS\"")
        ) {
            while (resultSet.next()) {
                String name = resultSet.getString("NAME");
                Integer size = resultSet.getInt("SIZE");
                Integer id = resultSet.getInt("ID");
                System.out.printf("NAME: %s, SIZE: %d, ID: %d%n", name, size, id);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
