package pl.zajecia26.database;

import java.sql.*;

public class PizzeriaDao {
    public static void main(String[] args) {

        database();

    }

    public static void database() {
        Connection connection = null;
        try {
            String url = "jdbc:postgresql://localhost:5432/postgres";
            connection = DriverManager.getConnection(url, "postgres", "postgres");

            Statement preparedStatement = connection.createStatement();
            ResultSet resultSet = preparedStatement.executeQuery("SELECT * FROM public.\"PIZZAS\"");


            while (resultSet.next()) {
                String name = resultSet.getString("NAME");
                Integer size = resultSet.getInt("SIZE");
                Integer id = resultSet.getInt("ID");
                System.out.printf("NAME: %s, SIZE: %d, ID: %d%n", name, size, id);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
