package pl.zajecia10.adnotacje;

public @interface YearsOpened {
    int since();

    int years();

    String firstOwner() default "Mcdonald";
}
