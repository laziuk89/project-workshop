package pl.zajecia10.adnotacje;

public @interface DodatkoweInformacjePizzeria {
    String address();
    boolean vegan() default false;
    String wlasciciel();
}
