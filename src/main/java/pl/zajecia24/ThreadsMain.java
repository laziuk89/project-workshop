package pl.zajecia24;

public class ThreadsMain {
    public static void main(String[] args) {

        //TODO: za pomocą strumieni zaprezentować działanie interfejsu funkcyjnych Predicate, Consumer, Function; dla wszystkich interfejsów użyc lambdy
        //TODO: użyc metody forEach() na liście

//
//        Thread downloader = new Downloader();
//        downloader.start();

//        Thread thread = new Thread(new Comptuing());
//        thread.start();
//        try {
//            Thread.sleep(3000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

        Thread lambdaThread = new Thread(() -> {

                System.out.println("running LAMBDA");
             try{
                 Thread.sleep(4000);
             }catch (InterruptedException e){
                 e.printStackTrace();
             }
        }
        );
        lambdaThread.start();
//        System.out.println(lambdaThread);
       try{
           lambdaThread.join();
       }catch (InterruptedException e){
           e.printStackTrace();
       }
        System.out.println("czy poczeka na ciebie?");
    }

    static class Downloader extends Thread {
        @Override
        public void run() {
            System.out.println("running thread");
        }
    }

    static class Comptuing implements Runnable {
        @Override
        public void run() {
//            System.out.println("starting Runnable");
            try{
                Thread.sleep(4000);
            }catch (InterruptedException e){
                e.printStackTrace();
            }

            System.out.println("running RUNNABLE");
        }
    }
}
