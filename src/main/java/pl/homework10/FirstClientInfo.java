package pl.homework10;

public @interface FirstClientInfo {
    int age();
    int id();
    String bank() default "European";
    boolean polish() default false;
}
