package pl.zajecia20.genericCollections;

import java.util.ArrayList;
import java.util.List;

public class GenericCollectionsMain {
    public static void main(String[] args) {

        List numbersRawList = new ArrayList();
        numbersRawList.add(new Integer(1));
        numbersRawList.add(new Integer(2));
        numbersRawList.add(new Integer(3));

        numbersRawList.add(new String("kiwi"));
        numbersRawList.add(new String("orange"));
        numbersRawList.add(new String("apple"));

      //  GenericCollectionsUtils.sumNumbersRawCollection(numbersRawList);

        List<Integer> numberList = new ArrayList<>();

        numberList.add(new Integer(0));
        numberList.add(new Integer(1));
        numberList.add(new Integer(4));
//        numberList.add(new String("kdjfbgd"));

      int sumNumbers = GenericCollectionsUtils.sumNumbers(numberList);
        System.out.println(sumNumbers);
    }
}
