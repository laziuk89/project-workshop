package pl.zajecia20.genericCollections;


import java.util.ArrayList;
import java.util.List;

public class GenericCollectionsWildCardsMain {

    public static void main(String[] args) {
        List<Object> objects = new ArrayList<>();

        objects.add(new Object());
        objects.add(new Object());
        objects.add(new String("animal"));
        objects.add(1);


        GenericCollectionsWildCardsUtils.printCollectionObjects(objects);

        List<String> strings = new ArrayList<>();

        strings.add("apple");
        strings.add("kiwi");

//typy generyczne w szczególności listy generyczne nic nie wiedzą o dziedziczeniu
//        GenericCollectionsWildCardsUtils.printCollectionObjects(strings);

        GenericCollectionsWildCardsUtils.printCollectionUnknown(objects);
        GenericCollectionsWildCardsUtils.printCollectionUnknown(strings);
        System.out.println("? extends");
        GenericCollectionsWildCardsUtils.printCollectionExtends(strings);
//        GenericCollectionsWildCardsUtils.printCollectionExtendsNumber(strings);
        List<Integer> integers = new ArrayList<>();
        integers.add(1);
        integers.add(2);
        integers.add(3);

        GenericCollectionsWildCardsUtils.printCollectionExtendsNumber(integers);

        List<DiligentStudent> diligentStudents = new ArrayList<>();
        GenericCollectionsWildCardsUtils.printCollectionSuperStudent(diligentStudents);
        List<NowakStudent> nowakStudents = new ArrayList<>();
//        GenericCollectionsWildCardsUtils.printCollectionSuperStudent(nowakStudents);

        List<Student> students = new ArrayList<>();
        GenericCollectionsWildCardsUtils.printCollectionSuperStudent(students);

        List<LazyStudent> lazyStudents = new ArrayList<>();
//        GenericCollectionsWildCardsUtils.printCollectionSuperStudent(lazyStudents);
    }
}
