package pl.zajecia20.genericCollections.homework;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        List<Pizza> pizzas = new ArrayList<>();
        pizzas.add(new Pizza("hawajska", 30));
        pizzas.add(new Pizza("vege", 20));
        pizzas.add(new Pizza("pepperoni", 40));



        List<Pizza> pizzas1 = new ArrayList<>();

        Pizza pizza1 = new Pizza("vege", 333);
        Pizza pizza2 = new Pizza("miesna", 111);
        pizzas1.add(pizza1);
        pizzas1.add(pizza2);

        PizzaCollectionsUtils.printPizzasList(pizzas);
        PizzaCollectionsUtils.printPizzasList(pizzas1);

        List<VeganPizza> veganPizzas = new ArrayList<>();
        veganPizzas.add(new VeganPizza("vegan", 40));
        veganPizzas.add(new VeganPizza("veganMaster", 30));

        PizzaCollectionsUtils.printPizzasListWildCardExtends(veganPizzas);

        List<RawVeganPizza> rawVeganPizzas = new ArrayList();
        rawVeganPizzas.add(new RawVeganPizza("superRawVegan", 20));
        rawVeganPizzas.add(new RawVeganPizza("megaRawVegan", 50));

        PizzaCollectionsUtils.printPizzasListWildCardExtends(rawVeganPizzas);
    }
}
