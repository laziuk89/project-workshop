package pl.zajecia20.genericCollections.homework;

import java.util.ArrayList;
import java.util.List;

public class PizzaCollectionsUtils {



    public static void printPizzasList(List<Pizza> pizzas){
        for (Pizza pizza : pizzas) {
            System.out.println(pizza);
        }
    }

    public static void printPizzasListWildCardExtends(List<? extends Pizza> pizzas){
        for (Pizza pizza : pizzas) {
            System.out.println(pizza);
        }
    }
}
