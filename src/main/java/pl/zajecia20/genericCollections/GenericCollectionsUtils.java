package pl.zajecia20.genericCollections;

import java.util.List;

public class GenericCollectionsUtils {

    public static void sumNumbersRawCollection(List numbersList) {
        Integer numbers = new Integer(0);
        for (Object object : numbersList) {
            System.out.println(object);
            //numbers= numbers + 10
            numbers += (Integer)object;
        }
    }

    public static  int sumNumbers(List<Integer> numbersList){
            Integer sumNumber = 0;
        for (Integer number : numbersList){
            System.out.println(number);
            sumNumber += number;
        }
        return sumNumber;
    }
}
