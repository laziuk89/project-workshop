package pl.zajecia20.genericCollections;

import java.util.List;

public class GenericCollectionsWildCardsUtils {
    ///<T> - type parameter (typ parametru)
    ///<?> - unknown type
    ///<Object> - type argument (typ argumentu)
    ///list<Object> objects - parametrized type (typ sparametryzowany)
    public static void printCollectionObjects(List<Object> objects) {
        for (Object object : objects) {
            System.out.println(object);
        }
    }
    public static void printCollectionUnknown(List<?> objects) {
        for (Object object : objects) {
            System.out.println(object);
        }
    }
    public static void printCollectionExtends(List<? extends Object> objects) {
        for (Object object : objects) {
            System.out.println(object);
        }
    }
    public static void printCollectionExtendsNumber(List<? extends Number> objects) {
        for (Number object : objects) {
            System.out.println(object);
        }
    }
    public static void printCollectionSuperStudent(List<? super DiligentStudent> objects) {
        System.out.println(objects);

    }
}
