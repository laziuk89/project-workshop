package pl.homework12;

public class Table {
    private int tableNumber;
    private Waiter waiter;

    public Table(int tableNumber, Waiter waiter) {
        this.tableNumber = tableNumber;
        this.waiter = waiter;
    }

    public int getTableNumber() {
        return tableNumber;
    }

    public void setTableNumber(int tableNumber) {
        this.tableNumber = tableNumber;
    }

    public Waiter getWaiter() {
        return waiter;
    }

    public void setWaiter(Waiter waiter) {
        this.waiter = waiter;
    }

    @Override
    public String toString() {
        return "Table{" +
                "tableNumber=" + tableNumber +
                ", waiter=" + waiter +
                '}';
    }
}
