package pl.homework12;


import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        List<Table> tables = new ArrayList<>();

        Waiter krzysiek = new Waiter("krzysiek", 40, Nationality.POLISH);
        Table table1 = new Table(1, krzysiek);

        Waiter john = new Waiter("John", 20, Nationality.ENGLISH);
        Table table13 = new Table(13, john);

        Waiter alexis = new Waiter("Alexis", 64, Nationality.FRENCH);
        Table table23 = new Table(23, alexis);



        tables.add(table1);
        tables.add(table13);
        tables.add(table23);
     //   tables.add(krzysiek);  // nie bedzie działać

        //System.out.println(tables);

        for (Table table : tables) {
            System.out.println("stoliki i obsługujący je kelnerzy " + table);
            if (table != null) {
                Waiter waiter = table.getWaiter();
//                if (table.getWaiter().getNationality() == Nationality.ENGLISH) { //to samo co w linijce niżej
                if (waiter.getNationality() == Nationality.FRENCH) {
                    System.out.println("kelner i stolik nr 13" + table.getWaiter() + waiter.getName());
                }
            }
        }
    }
}
