package pl.zajecia23.iterator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ForLoopVsIteratorMain {
    public static void main(String[] args) {
        List<Pizza> pizzas = new ArrayList<>();
        pizzas.add(new Pizza("vegan", 32));
        pizzas.add(new Pizza("hawajska", 42));
        pizzas.add(new Pizza("pepperoni", 50));
        pizzas.add(new Pizza("pepperoni", 50));
        pizzas.add(new Pizza("pepperoni", 50));

        for (Pizza pizza : pizzas) {
            System.out.println(pizza);
            if (pizza.getSize() > 40) {
                System.out.println("very large pizza " + pizza);
                pizzas.remove(pizza);
            }
        }

//        Iterator<Pizza> iterator = pizzas.iterator();
//        System.out.println("pizzas before remove " + pizzas.size());
//        while (iterator.hasNext()) {
//            Pizza pizza = iterator.next();
//            System.out.println(pizza);
//            if (pizza.getSize()>40){
//                System.out.println("very large pizza " + pizza);
//                iterator.remove();
//            }
//        }
//        System.out.println("pizzas after remove " + pizzas.size());
//        System.out.println(pizzas);
    }

    static class Pizza {
        private String name;
        private int size;

        public Pizza(String name, int size) {
            this.name = name;
            this.size = size;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        @Override
        public String toString() {
            return "Pizza{" +
                    "name='" + name + '\'' +
                    ", size=" + size +
                    '}';
        }
    }
}
