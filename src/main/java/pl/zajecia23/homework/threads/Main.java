package pl.zajecia23.homework.threads;

public class Main {
    public static void main(String[] args) {

        Mobile mobile = new Mobile();
        mobile.start();


        Thread thread = new Thread(new Computer());
        thread.start();

    }
    static class Mobile extends Thread{

        @Override
        public void run(){
            System.out.println("Mobile texting");
        }
    }

    static class Computer implements Runnable{

        @Override
        public void run() {
            System.out.println("computer playing");
        }
    }
}
