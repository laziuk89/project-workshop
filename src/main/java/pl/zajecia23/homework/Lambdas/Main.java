package pl.zajecia23.homework.Lambdas;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;

public class Main {
    public static void main(String[] args) {

        List<Boxer> boxers = asList(
                new Boxer("Adam", "Polish", 40, Style.DEFENSIVE),
                new Boxer("John", "American", 25, Style.DEFENSIVE),
                new Boxer("Stan", "English", 35, Style.OFFENSIVE),
                new Boxer("Stan", "English", 19, Style.OFFENSIVE)
        );

        List<Boxer> boxersStream = boxers.stream()
                .filter(boxer -> boxer.getAge() > 30)
                .filter(new BoxersFilter())
                .collect(Collectors.toList());

//        for (Boxer boxer : boxersStream) {
//            System.out.println(boxer);
//        }
        boxersStream.forEach(System.out::println);
    }


    static class Boxer {
        private String name;
        private String nationality;
        private int age;
        private Style style;

        public Boxer(String name, String nationality, int age, Style style) {
            this.name = name;
            this.nationality = nationality;
            this.age = age;
            this.style = style;
        }

        public String getName() {
            return name;
        }

        public String getNationality() {
            return nationality;
        }

        public int getAge() {
            return age;
        }

        public Style getStyle() {
            return style;
        }

        @Override
        public String toString() {
            return "Boxer{" +
                    "name='" + name + '\'' +
                    ", nationality='" + nationality + '\'' +
                    ", age=" + age +
                    ", style=" + style +
                    '}';
        }
    }

    enum Style {

        DEFENSIVE, OFFENSIVE;
    }

    static class BoxersFilter implements Predicate<Boxer> {

        @Override
        public boolean test(Boxer boxer) {
//            System.out.println(boxer);
            return boxer.getStyle() == Style.DEFENSIVE;
        }
    }

}