package pl.zajecia23.lambdas.parameters;

public enum TemperatureConvertionType {
    TO_CELSJUSZ, TO_FARHENHEIT;
}
