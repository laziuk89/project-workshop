package pl.zajecia23.lambdas.parameters;

public class StringDecoratorMain {
    public static void main(String[] args) {

        StringDecorator stringDecoratorWithImplementation = new UpperCaseStringDecorator();
        stringDecoratorWithImplementation.decorate("e", "cz", "ść");


        StringDecorator stringDecoratorWithAnonymousImplementation = new StringDecorator() {
            @Override
            public void decorate(String string, String prefix, String sufix) {
                System.out.printf("%s%s%s%n", prefix.length(), string, sufix.length());
            }
        };
        stringDecoratorWithAnonymousImplementation.decorate("kod", "pies", "ala");

//        public void decorate(String string, String prefix, String sufix) { ---->to samo w lambda = (String st, String pr, String su) -> {
        StringDecorator lambdaDecorator = (String st, String pr, String su) -> {
            System.out.printf("%s%s%s%n", pr.hashCode(), st, su.toLowerCase());
        };
        lambdaDecorator.decorate("jabłko", "banan", "KiWi");

        StringDecorator simpleLambdaDecorator = (st, pr, su) -> System.out.printf("%s%s%s%n", pr.hashCode(), st, su.toLowerCase());
        simpleLambdaDecorator.decorate("truskawka", "jagoda", "kot");
    }

    static class UpperCaseStringDecorator implements StringDecorator {
        @Override
        public void decorate(String string, String prefix, String sufix) {
            System.out.println("decorating string ");
            System.out.printf("%s%s%s%n", prefix.toUpperCase(), string, sufix.toUpperCase());
//            System.out.println(prefix.toUpperCase() + string + sufix.toUpperCase());
        }
    }
}
