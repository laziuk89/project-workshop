package pl.zajecia23.lambdas.parameters;

public class TemperatureConverterMain {
    public static void main(String[] args) {
        TemperatureConverter simpleTemperatureConverter = new SimpleTemperatureConverter();
        double farhenheit = simpleTemperatureConverter.convert(12.0, 0, TemperatureConvertionType.TO_FARHENHEIT);
        System.out.println(farhenheit);


        TemperatureConverter lambdaConverter = (c, f, conv) -> {
            System.out.printf("converting %f to %f using %s%n", c, f, conv);
            return 11.0;
        };
        double convertedLambda = lambdaConverter.convert(15.0, 50.5, TemperatureConvertionType.TO_CELSJUSZ);
        System.out.println(convertedLambda);

//        TemperatureConverter lambdaConverterWithoutReturn = (c, f, conv) -> {return c*f};
        TemperatureConverter lambdaConverterWithoutReturn = (c, f, conv) -> c * f;

        double convertedLambdaWithoutReturn = lambdaConverterWithoutReturn.convert(15.0, 50.5, TemperatureConvertionType.TO_CELSJUSZ);
        System.out.println(convertedLambdaWithoutReturn);
    }

    static class SimpleTemperatureConverter implements TemperatureConverter {

        @Override
        public double convert(double celsjusz, double farhenheit, TemperatureConvertionType convertionType) {
            System.out.println("converting");
            switch (convertionType) {
                case TO_FARHENHEIT: {
                    System.out.println("to Fahrenheit");
                    break;
                }
                case TO_CELSJUSZ: {
                    System.out.println("to Celsjusz");
                    break;
                }
                default: {
                    break;
                }
            }
            return 0;
        }
    }
}
