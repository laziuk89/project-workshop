package pl.zajecia23.lambdas.parameters;

@FunctionalInterface  //////////interface funkcyjny używany w połączeniu z lambdami; interface funkcyjny z definicji  to taki który posiada tylko i wyłącznie jedną metodę
                    ////////////adnotacja @FunctionalInterface może być użyta, ale nie musi dla interface'u funkcyjnego; adnotacja jest informacją dla kompilatora
public interface StringDecorator {

    void decorate(String string,String prefix, String sufix);

}
