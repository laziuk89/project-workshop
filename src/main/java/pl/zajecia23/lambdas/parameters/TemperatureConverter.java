package pl.zajecia23.lambdas.parameters;

@FunctionalInterface
public interface TemperatureConverter {

    double convert(double celsjusz, double farhenheit, TemperatureConvertionType convertionType);
}
