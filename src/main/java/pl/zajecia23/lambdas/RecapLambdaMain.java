package pl.zajecia23.lambdas;

public class RecapLambdaMain {
    public static void main(String[] args) {

        LambdaInterface lambdaInterface = new LambdaInterface() {
            @Override
            public void doLambda() {
                System.out.println("speaking from lambdaInterface ");
            }
        };
        lambdaInterface.doLambda();

        LambdaInterface lambda = ()->{
            System.out.println("speaking from lambda");
        };
        lambda.doLambda();
    }

    @FunctionalInterface
    interface   LambdaInterface {
        void doLambda();
    }
}
