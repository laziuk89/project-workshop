package pl.zajecia23.lambdas;

public class SimpleLambdaMain {
    public static void main(String[] args) {

//        do korzystania z lambdy potrzebujemy @FunctionalInterface
        SimpleFunctionalInterface simpleFunctionalInterface = new SimpleFunctionalInterface() {
            @Override
            public void speak() {
                System.out.println("anonymous speaking");
            }
        };

        simpleFunctionalInterface.speak();


        SuperSipmleFunctionalInterface superSipmleFunctionalInterface = new SuperSipmleFunctionalInterface();
        superSipmleFunctionalInterface.speak();

//        SimpleFunctionalInterface simpleFunctionalInterface = new SimpleFunctionalInterface() {
//            @Override
//            public void placeYourOrder() {
//                System.out.println("anonymous speaking");
//            }
//        };
        SimpleFunctionalInterface lambdaSimpleFunctionalInterface = () -> {
            System.out.println("lambda speaking");
        };
        lambdaSimpleFunctionalInterface.speak();
    }


    static class SuperSipmleFunctionalInterface implements SimpleFunctionalInterface {
        @Override
        public void speak() {
            System.out.println("super speaking");
        }
    }
}
