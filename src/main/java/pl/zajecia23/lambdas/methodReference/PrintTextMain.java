package pl.zajecia23.lambdas.methodReference;

public class PrintTextMain {
    public static void main(String[] args) {
        //TODO: przecwiczyc lambda dla interfejsów funkcyjnych których metody:
        // first. nic nie zwracają nic nie pryzjmują
        // 2.nic nie zwracjaą i przyjmują parametry
        // 3. zwracają i przyjmują parametry
        // dla powyzszych w odzielnych klasach z metoda main zrobic:
        // first.wewnętrzną klasę implementującą interfejs funkcyjny
        // 2.anonimową implementację interfejsu
        // 3.lambdę dla interfejsu funkcyjnego
        // przecwiczyc method reference na przykladzie sout
        // znaleźć w interenecie informację o istniejacych interfejsach funkcyjnych udostepnionych przez jezyk java
        // first.Consumer - metoda accept
        // 2.Function - metoda apply
        // 3. Predicate - metoda text
        // 4.Supplier - metoda get

        PrintingText printText = new SimplePrintingText();
        printText.printText("ala ma kota");

        PrintingText lambdaPrintingText = s -> System.out.println("Printing from lambda: " + s);
        lambdaPrintingText.printText("ala ma psa");

        PrintingText lambdaPrintingTextSimplePrint = s -> System.out.println(s);
        lambdaPrintingTextSimplePrint.printText("ala ma psa");
        PrintingText lambdaPrintingTextMethodReference = System.out::println;

        lambdaPrintingTextMethodReference.printText("ala ma psa");
    }

    static class SimplePrintingText implements PrintingText {

        @Override
        public void printText(String text) {
            System.out.println("printing text: " + text);
        }
    }
}
