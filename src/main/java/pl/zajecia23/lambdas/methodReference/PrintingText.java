package pl.zajecia23.lambdas.methodReference;

@FunctionalInterface
public interface PrintingText {
//      public void println(String x) {  ///////// sygnatura metody:
//      specyfikator dostępu - public
//      typ zwracany - void/String
//      nazwa metody - println/printText
//      parametry metody - (String x)
    void printText(String text);
}
