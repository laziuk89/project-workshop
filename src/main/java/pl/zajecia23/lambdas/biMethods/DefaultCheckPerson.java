package pl.zajecia23.lambdas.biMethods;

public class DefaultCheckPerson implements CheckPerson {
    @Override
    public boolean test(Person person) {
//        if (person.getGender() == Pet.Gender.MALE
//                && person.getAge() >= 18
//                && person.getAge() <= 25) {
//            return true;
//        }
//        return false;

        return person.getGender() == Person.Gender.MALE
                && person.getAge() >= 18
                && person.getAge() <= 25;
    }
}
