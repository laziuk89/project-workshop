package pl.zajecia23.lambdas.biMethods;

import java.util.List;
import java.util.function.Predicate;

public class PersonUtils {
    public static void printPersonsOlderThan(List<Person> peopleList, int age) {
        for (Person person : peopleList) {
            if (person.getAge() >= age) {
                person.printPerson();
            }
        }
    }

    public static void printPersonsWithinAgeRange(List<Person> peopleList, int low, int high) {
        for (Person person : peopleList) {
            if (low <= person.getAge() && person.getAge() < high) {
                person.printPerson();
            }
        }
    }

    public static void printPersonsWithCondition(List<Person> peopleList, CheckPerson checkPerson) {
        for (Person person : peopleList) {
            if (checkPerson.test(person)) {
                person.printPerson();
            }
        }
    }
    public static void printPersonsWithPredicate(List<Person> peopleList, Predicate<Person> predicate) {
            for (Person person : peopleList) {
            if (predicate.test(person)) {
                person.printPerson();
            }
        }
    }
    public static void printPizzasWithPredicate(List<Pizza> pizzas, Predicate<Pizza> predicate) {
        for (Pizza pizza : pizzas) {
            if (predicate.test(pizza)) {
                pizza.printPizza();
            }
        }
    }
    //TODO: na wzór print pizzasWithPredicate przecwiczyc inne interfejsy funkcyjne z javy: Consumer, Function, Supplier
}
