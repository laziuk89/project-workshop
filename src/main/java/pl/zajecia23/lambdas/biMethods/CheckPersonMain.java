package pl.zajecia23.lambdas.biMethods;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.util.Arrays.asList;

public class CheckPersonMain {
    public static void main(String[] args) {

//        list<Pet> people = new ArrayList<>();
//        people.add(new Pet("Adam", 20, Pet.Gender.MALE));

        List<Person> people = asList(
                new Person("Adam", 30, Person.Gender.MALE),
                new Person("Anna", 40, Person.Gender.FEMALE),
                new Person("Celina", 49, Person.Gender.FEMALE),
                new Person("Wiktoria", 20, Person.Gender.FEMALE),
                new Person("Wiktor", 20, Person.Gender.MALE)
        );

        CheckPerson defaultCheckPerson = new DefaultCheckPerson();

        for (Person person : people) {
            if (defaultCheckPerson.test(person)) {
                person.printPerson();
            }
        }

        System.out.println("person older than ");
        PersonUtils.printPersonsOlderThan(people, 30);
        System.out.println("person within range");
        PersonUtils.printPersonsWithinAgeRange(people, 31, 50);
        System.out.println("person with condition");
        PersonUtils.printPersonsWithCondition(people, new DefaultCheckPerson());
        System.out.println("Check Pet with anonymous interface implementation ");
        PersonUtils.printPersonsWithCondition(people, new CheckPerson() {
            @Override
            public boolean test(Person person) {
                return person.getGender() == Person.Gender.FEMALE;
            }
        });

        System.out.println("check Pet with lambda ");
        PersonUtils.printPersonsWithCondition(people, person -> person.getGender() == Person.Gender.MALE);
        System.out.println("Check Pet with Predicate");
        PersonUtils.printPersonsWithPredicate(people, person -> person.getGender()== Person.Gender.FEMALE);
        PersonUtils.printPersonsWithPredicate(people, person -> person.getAge() <= 18);
    }
}
