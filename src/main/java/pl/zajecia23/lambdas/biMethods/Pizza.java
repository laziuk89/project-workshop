package pl.zajecia23.lambdas.biMethods;

public class Pizza {
    private int size;
    private String name;

    public Pizza(int size, String name) {
        this.size = size;
        this.name = name;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "size=" + size +
                ", name='" + name + '\'' +
                '}';
    }

    public void printPizza() {
        System.out.println(this);
    }
}
