package pl.zajecia23.lambdas.homework.second;

public interface RandomMethod {
    void speak(String text);
}
