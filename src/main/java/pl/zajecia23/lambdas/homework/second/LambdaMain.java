package pl.zajecia23.lambdas.homework.second;

public class LambdaMain {
    public static void main(String[] args) {

        RandomMethod randomMethod = System.out::println;
        randomMethod.speak("leel");

        OtherInterface otherInterface = (status) -> System.out.println("your status is " + status);
        otherInterface.giveStatus(" SINGLE ");

        Thirdnterface thirdnterface = System.out::println;
        System.out.println("eloleleo");
    }
}
