package pl.zajecia23.lambdas.homework.first;

public interface BarWithParameteres {
    void orderWithParameters(int bill);
}
