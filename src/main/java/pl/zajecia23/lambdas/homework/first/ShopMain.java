package pl.zajecia23.lambdas.homework.first;

public class ShopMain {

    public static void main(String[] args) {


        Shop shopInnerClass = new Shop() {
            @Override
            public void withoutparameters() {
                System.out.println("text");
            }

            @Override
            public void placeYourOrder(int orderNumber) {
                System.out.println("text ");
            }

            @Override
            public void fullParameteres(int example) {
                System.out.println("text " + example);
            }
        };

        Restaurant restaurant = () -> {
            System.out.println("our menu is ");

        };

        BarWithParameteres barWithParameteres = bill -> {
            System.out.println("your order is ");
        };
        BardWithFullParameters bardWithFullParameters = numberOfDishes -> {
            System.out.println("the number of your dishes is " + numberOfDishes);
            return numberOfDishes;
        };
        BardWithFullParameters parametersAndReturnType = numberOfDishes -> Integer.valueOf(numberOfDishes);

        BardWithFullParameters parametersAndReturnTypeWithMethodReference = Integer::valueOf;
        int fullParameters = parametersAndReturnTypeWithMethodReference.orderFullParameters(30);
        System.out.println(fullParameters);
    }
}

