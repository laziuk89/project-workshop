package pl.zajecia23.lambdas.homework.first;

public interface BardWithFullParameters {
    int orderFullParameters(int numberOfDishes);
}
