package pl.zajecia23.streams;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;

public class StreamsMain {
    public static void main(String[] args) {

        List<Notebook> notebooks = asList(
                new Notebook(200, 4, "jaś i małgosia", Cover.HARD),
                new Notebook(340, 50, "czerwony kapture", Cover.HARD),
                new Notebook(150, 99, "harry potter", Cover.SOFT),
                new Notebook(80, 20, "lotr", Cover.SOFT)
        );

        List<Notebook> notebooksStream = notebooks.stream()
                .filter(notebook -> notebook.getCover().equals(Cover.HARD))
//                .filter(notebook -> notebook.getTitle().startsWith("J".toLowerCase()))
                .filter(new NotebookFilter())
                .collect(Collectors.toList());

        for (Notebook notebook : notebooksStream) {
            System.out.println(notebook);
        }

        List<String> notebooksString = notebooks.stream()
//                .map(new NotebookStreamMap())
                .map(notebook -> notebook.getTitle() + " " + notebook.getCover())
                .collect(Collectors.toList());

//        for (String string : notebooksString) {
//            System.out.println(string);
//        }
//        notebooksString.forEach(s -> System.out.println(s));
        notebooksString.forEach( System.out::println);

//        notebooks.forEach(new NotebookForEach());
        notebooks.forEach(notebook -> System.out.println("for each " + notebook));

//        notebooksString.forEach(s -> System.out.println(s.toUpperCase()));

    }


    static class NotebookForEach implements Consumer<Notebook> {

        @Override
        public void accept(Notebook notebook) {
            System.out.println("accepting " + notebook);
        }
    }

    static class NotebookStreamMap implements Function<Notebook, String> {

        @Override
        public String apply(Notebook notebook) {
            return notebook.getTitle() + " " + notebook.getCover();
        }
    }

    static class NotebookFilter implements Predicate<Notebook> {

        @Override
        public boolean test(Notebook notebook) {
            return notebook.getCurrentPage() < 50;
        }
    }

    static class Notebook {

        private Integer pageNumbers;
        private Integer currentPage;
        private String title;
        private Cover cover;

        public Notebook(Integer pageNumbers, Integer currentPage, String title, Cover cover) {
            this.pageNumbers = pageNumbers;
            this.currentPage = currentPage;
            this.title = title;
            this.cover = cover;
        }

        public Integer getPageNumbers() {
            return pageNumbers;
        }

        public void setPageNumbers(Integer pageNumbers) {
            this.pageNumbers = pageNumbers;
        }

        public Integer getCurrentPage() {
            return currentPage;
        }

        public void setCurrentPage(Integer currentPage) {
            this.currentPage = currentPage;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public Cover getCover() {
            return cover;
        }

        public void setCover(Cover cover) {
            this.cover = cover;
        }

        @Override
        public String toString() {
            return "Notebook{" +
                    "pageNumbers=" + pageNumbers +
                    ", currentPage=" + currentPage +
                    ", title='" + title + '\'' +
                    ", cover=" + cover +
                    '}';
        }
    }

    static enum Cover {

        SOFT, HARD

    }
}
