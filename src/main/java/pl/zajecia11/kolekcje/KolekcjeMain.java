package pl.zajecia11.kolekcje;


import java.util.*;

public class KolekcjeMain {
    public static void main(String[] args) {
        String[] tablica = new String[10];
        tablica[6] = "kot";//lista.add("kot");
        //tablica[11]="pies";
        //lista.add("pies");
//        for (int i = 0; i < tablica.length; i++) {
//            tablica[i] = " ";
//
//        }

        String[] tmp = Arrays.copyOf(tablica, tablica.length + 1);
        List<String> strings = Arrays.asList(tablica);
        System.out.println(strings);


        String element0 = tablica[0];//lista.get(0);

//        tablica[0] = null;//lista.remove("kot");
        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] != null) {

                if (tablica[i].equals("kot")) {
//                tablica[i]=null;
                    System.out.println("jestem kotem " + i);
                }
            }
        }

        List<String> lista = new ArrayList<>();
        lista.add("hello lista");
        lista.add("pies");
        lista.add("pies");
        lista.add(null);

        lista.remove("pies");
        System.out.println(lista.get(1));//wypisuje dany elemen
        System.out.println(lista.get(0));//wypisuje dany element
        System.out.println(lista.size());//podaje rozmiar

        //typ jest string'iem, nie mogę dać nic innego
        // lista.add(4);

        List<String> vectors = new Vector<>();
        vectors.add("hello vectors");
        vectors.add("kot");
        vectors.add("kot");
        vectors.add(null);

        vectors.remove("kot");

        System.out.println(lista);
        System.out.println(vectors);

        Set<String> set = new HashSet<>();
        set.add("hello set");
        set.add("mysz");
        set.add("mysz");
        set.add("pies");
        set.add(null);
        set.add(null);

        set.remove("mysz");

        System.out.println(set);

        Queue<String> queue = new PriorityQueue<>();
        queue.add("czesc");
        queue.add("czesc");

        queue.remove("czesc");
        queue.offer("witam queue");

        System.out.println(queue);

        Collection c = null;

    }
}
