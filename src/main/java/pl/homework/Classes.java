package pl.homework;

public class Classes {

    private String species;
    private int quantity;

    public String getSpecies() {
        return species;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }


    public void setSpecies(String species) {
        this.species = species;


    }

    @Override
    public String toString() {
        return "Classes{" +
                "species='" + species + '\'' +
                ", quantity=" + quantity +
                '}';
    }
}
