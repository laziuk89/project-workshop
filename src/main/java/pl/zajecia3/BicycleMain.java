package pl.zajecia3;

public class BicycleMain {
    public static void main(String[] args) {
        Bicycle romet = new Bicycle(2);
//        romet.WHEELS_NUMBER=3;

        //pełna nazwa pakietowa klasy
//        pl.laziuk.maciej.zajecia.java.zajecia3.Bicycle veturilo = new Bicycle(7);
//        pl.laziuk.maciej.zajecia.java.zajecia3.rowery.Bicycle rower = new pl.laziuk.maciej.zajecia.java.zajecia3.rowery.Bicycle();

        Bicycle bicycle = new Bicycle(10);
        Bicycle.Wheel wheel = bicycle.new Wheel("nowe");

        Bicycle.Break bicycleBreak = new Bicycle.Break(100);

        SuperBicycle superBicycle = new SuperBicycle();

        //Computer anonimowa
        Bicycle anonymousBicycle = new Bicycle(2) {
            private int speed;

            public void speak(int speed) {
                System.out.println();
            }

            @Override
            public String toString() {
                return "$classname{" +
                        "speed=" + speed +
                        "} " + super.toString();
            }
        };
    }
}
