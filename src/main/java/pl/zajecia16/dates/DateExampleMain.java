package pl.zajecia16.dates;

import java.util.Date;

public class DateExampleMain {
    public static void main(String[] args) {

        Date firstDate = new Date();

        System.out.println(firstDate);

        Date secondDate = new Date(1989, 12, 10);
        System.out.println(secondDate);

        Date thirdDate = new Date(0L);
        System.out.println(thirdDate);

        Date fouthdDate = new Date(1000L);
        System.out.println(fouthdDate );

        Date fifthdDate = new Date();
        fifthdDate.setTime(1_000_000L);
        System.out.println(fifthdDate );


    }
}
