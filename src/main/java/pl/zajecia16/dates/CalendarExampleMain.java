package pl.zajecia16.dates;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

public class CalendarExampleMain {
    public static void main(String[] args) {

        Calendar calendar = Calendar.getInstance(); // mutable

        //////////////ctrl+shift+spacja - @MagicConstant
        System.out.println(calendar.get(Calendar.HOUR));
        System.out.println(calendar.get(Calendar.AM_PM));
        calendar.add(Calendar.HOUR, 10);
        System.out.println(calendar.get(Calendar.HOUR));

        System.out.println("BigDecimal");
        BigDecimal bigDecimal = BigDecimal.ZERO;//niezmienialny immutable
        System.out.println(bigDecimal);
        BigDecimal bigDecimalOne = bigDecimal.add(BigDecimal.ONE);
        System.out.println(bigDecimalOne);
        System.out.println(bigDecimal);

        System.out.println("back to calendar");

        Calendar calendar2 = Calendar.getInstance();
        System.out.println(calendar2);
        calendar2.set(2000, Calendar.AUGUST, 0);
        System.out.println(calendar2.getTime());
        System.out.println(calendar2.getTimeInMillis());

        Calendar calendar3 = Calendar.getInstance(TimeZone.getTimeZone("US/Arizona"),Locale.US);
        System.out.println(calendar3.getTime());

        boolean afterCalendar = calendar.after(calendar2);
        System.out.println(afterCalendar);
        int compareCalendar =  calendar.compareTo(calendar2);
        System.out.println(compareCalendar);

    }
}
