package pl.zajecia14.wyjatki;

import java.util.LinkedList;
import java.util.List;

public class PizzeriaMain {
    public static void main(String[] args) {

        LinkedList<Pizza> pizzas = new LinkedList<>();
        pizzas.add(new Pizza("margarita", 30));
        pizzas.add(new Pizza("funghi", 50));

        Pizzeria pizzeria1 = new Pizzeria(pizzas);
        try {

            pizzeria1.checkAvailablePizzas();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        try {

            pizzeria1.takeOrder(new Pizza("margarita", 30));
            pizzeria1.takeOrder(new Pizza("carbonara", 30));
            pizzeria1.takeOrder(new Pizza("vege", 30));
        } catch (NoPizzaException e) {
            System.out.println(e.getMessage());
        }


    }
}
