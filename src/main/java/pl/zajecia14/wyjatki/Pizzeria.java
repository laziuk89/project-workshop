package pl.zajecia14.wyjatki;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

public class Pizzeria {

    private LinkedList<Pizza> pizzas;

    public Pizzeria(LinkedList<Pizza> pizzas) {
        this.pizzas = pizzas;
    }

    public void takeOrder(Pizza pizza) throws NoPizzaException {
        try {

            this.pizzas.removeFirst();
        } catch (NoSuchElementException e) {
            // e.printStackTrace();
            throw new NoPizzaException("brak pizzy ", e);
        }
        System.out.println(this.pizzas.size() + " pizzas left");
    }

    public void checkAvailablePizzas() throws NoPizzaException {
        System.out.println(" checking available pizzas ");
        if (pizzas.size() == 0) {
            throw new NoPizzaException("brak pizzy", null);
        } else {
            System.out.println(pizzas.size() + " pizzas left");
        }
    }

    public LinkedList<Pizza> getPizzas() {
        return pizzas;
    }
}
