package pl.zajecia14.wyjatki;

public class NoPizzaException extends Exception {

    public NoPizzaException(String message, Throwable cause) {
        super(message, cause);
    }
}
