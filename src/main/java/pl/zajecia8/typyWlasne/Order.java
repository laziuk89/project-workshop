package pl.zajecia8.typyWlasne;

public class Order {

    private String tableName;
    private Pizza orderedPizza;

    public Order(String tableName, Pizza orderedPizza) {
        this.tableName = tableName;
        this.orderedPizza = orderedPizza;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public Pizza getOrderedPizza() {
        return orderedPizza;
    }

    public void setOrderedPizza(Pizza orderedPizza) {
        this.orderedPizza = orderedPizza;
    }

    @Override
    public String toString() {
        return "order{" +
                "tableName='" + tableName + '\'' +
                ", orderedPizza=" + orderedPizza +
                '}';
    }
}
