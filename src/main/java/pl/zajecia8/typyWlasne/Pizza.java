package pl.zajecia8.typyWlasne;

public class Pizza {

    private String name;
    private int size;
    private PizzaType type; //własny typ zmiennej PizzaType

    public Pizza() {
    }

    /**
     *
     * @param name
     * @param size
     * @param type
     */
    public Pizza(String name, int size, PizzaType type) { //przekazywanie parametru który jest naszego własnego typu PizzaType
        this.name = name;
        this.size = size;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public PizzaType getType() {
        return type;
    }

    public void setType(PizzaType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "name='" + name + '\'' +
                ", size=" + size +
                ", type=" + type +
                '}';
    }
}
