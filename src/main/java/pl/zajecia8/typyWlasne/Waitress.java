package pl.zajecia8.typyWlasne;

import java.util.ArrayList;
import java.util.List;

public class Waitress {
    private String name;
    private  int age;
    //////// WAZNE
    private List<Order> orders=new ArrayList<>();

    public Waitress(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void takeOrder(String tableName, Pizza orderedPizza){
        Order waitressOrder = new Order(tableName, orderedPizza);
        this.orders.add(waitressOrder);
    }

    @Override
    public String toString() {
        return "Waitress{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", orders=" + orders +
                '}';
    }
}
