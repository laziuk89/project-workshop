package pl.zajecia8.typyWlasne;

public class PizzeriaMain {
    public static void main(String[] args) {

        Pizza margarita = new Pizza("margerita", 30, PizzaType.NORMAL);

        System.out.println(margarita);

        Order table21 = new Order("table 21", margarita);
        System.out.println(table21);

        Pizza funghi = new Pizza("funghi", 45, PizzaType.VEGAN);
        Waiter jan = new Waiter("Jan", 36);

        //order - typ zmiennej;
        //orderedFunghi - nazwa zmiennej;
        //= jan.takeOrder(...) - przypisanie wartości zwracanej z metody do zmiennej orderedFunghi
        Order orderedeFunghi = jan.takeOrder("table 3", funghi);
        System.out.println(orderedeFunghi);

        Waitress basia = new Waitress("Basia", 18);
        basia.takeOrder("table 99", margarita);
        System.out.println(basia);

        basia.takeOrder("table 99", funghi);
        System.out.println(basia);

    }
}
