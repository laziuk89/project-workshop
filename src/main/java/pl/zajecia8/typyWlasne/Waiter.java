package pl.zajecia8.typyWlasne;

public class Waiter {

    private String name;
    private int age;
    private Order order;

    public Waiter(String name, int age) {
        this.name = name;
        this.age = age;
    }

    /**
     * metoda obsługująca przyjmowanie zamówienia
     * @param tableName numer stolika dla którego przyjęto zamówienie
     * @param pizza zamówiona pizza
     * @return zwraca zamówienie
     */
    public Order takeOrder(String tableName, Pizza pizza) { //przekazywanie parametru metody którjest naszego własnego typu
//        System.out.println("przyjmuje zamówienie do stolika " + tableName);
//        System.out.println("zamówiono " + pizza);
        Order order = new Order(tableName, pizza);
      //  System.out.println("zamówienie: " + order);
        return order;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    @Override
    public String toString() {
        return "Waiter{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", order=" + order +
                '}';
    }
}
