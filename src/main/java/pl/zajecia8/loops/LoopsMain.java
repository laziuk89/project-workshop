package pl.zajecia8.loops;

import java.util.Arrays;

public class LoopsMain {
    public static void main(String[] args) {
        String[] animals = new String[5];

        animals[0]="cat";
        animals[1]="dog";

        System.out.println(animals);
        System.out.println(animals[0]);
        System.out.println(Arrays.asList(animals));

        //pętla z indeksem
        //int i = 0; - zmienna po której będziemy iterować, przechodzić przez kolejne elementy; i jest indeksem
        //i <animals.length; - warunek zakończenia pętli
        //i++ - zmiana wartości indeksu, zmniejszenie lub zwiększenie indeksu
        for(int i=0; i<animals.length; i++){
            System.out.println("zwierzę: " + animals[i]);
        }
        for(;;){
            System.out.println("jestem w pętli nieskonczonej");
        }

    }
}
