package pl.zajecia25.java_swing;

import javax.swing.*;
import java.awt.*;

public class FirstWindow {
    public static void main(String[] args) {
        JFrame mainWindow = new JFrame();
        mainWindow.setTitle("główne okno");
        mainWindow.setSize(new Dimension(300, 300));
        mainWindow.setVisible(true);
    }
}
