package pl.zajecia25.java_swing;

import javax.swing.*;
import java.awt.*;

public class SecondWindow extends JFrame {
    public static void main(String[] args) {

        new SecondWindow();

    }

    public SecondWindow() {
        setTitle("Drugie główne okienko");
        setSize(new Dimension(500, 750));
        setVisible(true);
    }
}
