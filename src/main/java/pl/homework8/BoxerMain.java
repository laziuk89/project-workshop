package pl.homework8;

public class BoxerMain {
    public static void main(String[] args) {


        Boxer floyd = new Boxer(250000, "Floyd", "ducking");
        floyd.setNationality(Nationality.AMERICAN);
        Nationality nationality1 = floyd.getNationality();
        speakSwitch(nationality1);

        Boxer joshua = new Boxer(300000, "Joshua", "uppercut");
        joshua.setNationality(Nationality.ENGLISH);
        speakSwitch(joshua.getNationality());

        Boxer golota = new Boxer(550, "Golota", "jab", Nationality.POLISH);
        speakSwitch(golota.getNationality());
    }

    public static void speakSwitch(Nationality nationality) {
        switch (nationality) {
            case ENGLISH: {
                System.out.println("boxer's nationality is: " + nationality.getName());
                break;
            }
            case AMERICAN: {
                System.out.println("boxer's nationality is: " + nationality.getName());
                break;
            }
            case POLISH: {
                System.out.println("boxer's nationality is: " + nationality.getName());
                break;
            }
            default: {
                System.out.println("don't know the nationality");
                break;
            }
        }
    }

}
