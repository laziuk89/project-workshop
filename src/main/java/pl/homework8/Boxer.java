package pl.homework8;

public class Boxer {
    private int wage;
    private String name;
    private String specialMove;
    private Nationality nationality;

    public Boxer() {
    }

    public Boxer(int wage, String name, String specialMove) {
        this.wage = wage;
        this.name = name;
        this.specialMove = specialMove;
    }

    public Boxer(int wage, String name, String specialMove, Nationality nationality) {
        this.wage = wage;
        this.name = name;
        this.specialMove = specialMove;
        this.nationality = nationality;
    }

    public int getWage() {
        return wage;
    }

    public Nationality getNationality() {
        return nationality;
    }

    public void setWage(int wage) {
        this.wage = wage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecialMove() {
        return specialMove;
    }

    public void setSpecialMove(String specialMove) {
        this.specialMove = specialMove;
    }

    public void setNationality(Nationality nationality) {
        this.nationality = nationality;
    }

    @Override
    public String toString() {
        return "Boxer{" +
                "wage=" + wage +
                ", name='" + name + '\'' +
                ", specialMove='" + specialMove + '\'' +
                ", nationalit='" + nationality+ '\'' +
                '}';
    }
}
