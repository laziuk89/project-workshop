package pl.homework8;

public enum Nationality {

    AMERICAN("American"), ENGLISH("English"), POLISH("Polish");

    private String name;

    Nationality() {
    }

    Nationality(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
