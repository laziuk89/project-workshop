package pl.zajecia22.streamsio;

import java.io.*;

public class BufferedStreamsMain {
    public static void main(String[] args) {

        try (
                BufferedReader bufferedReader = new BufferedReader(new FileReader(new File("D:\\dane-bufor.txt")));
                BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("D:\\dane-bufor-kopia.txt"))
        ) {
//            int i;
//            while ((i=bufferedReader.read())!=-first){
//                bufferedWriter.write(i);
//            }
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                bufferedWriter.write("3XXX \n\r "+line);
                bufferedWriter.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
