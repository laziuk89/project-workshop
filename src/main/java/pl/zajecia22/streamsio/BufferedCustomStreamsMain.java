package pl.zajecia22.streamsio;

import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class BufferedCustomStreamsMain {
    //TODO: w projekcie z przychodnią dodać metodę która będzie zapisywać dane z grafiki(listę wszystkich wizyt wygenerowanych dla danego miesiąca); nową metodę umieścić w klasie utilowej
    public static void main(String[] args) {
        List<Visit> visits = new ArrayList<>();
        visits.add(new Visit(LocalDate.now(), new Patient("Jacek", "123456789")));
        visits.add(new Visit(LocalDate.now(), new Patient("Maciek", "123000089")));
        visits.add(new Visit(LocalDate.now(), new Patient("Piotrek", "100000000")));


        try (
//                BufferedReader bufferedReader = new BufferedReader(new FileReader(new File("D:\\dane-bufor.txt")));
//                BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("D:\\dane-bufor-kopia.txt"))
                BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("D:\\visits.txt"))
        ) {
//            int i;
//            while ((i=bufferedReader.read())!=-first){
//                bufferedWriter.write(i);
//            }
//            String line;
//            while ((line = bufferedReader.readLine()) != null) {
//                bufferedWriter.write("3XXX \n\r " + line);
//                bufferedWriter.newLine();
//            }
            for(Visit visit : visits){
                bufferedWriter.write(visit.toString());
                bufferedWriter.newLine();
            }
//            bufferedWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static class Visit {
        private LocalDate localDate;
        private Patient patient;

        public Visit(LocalDate localDate, Patient patient) {
            this.localDate = localDate;
            this.patient = patient;
        }

        public LocalDate getLocalDate() {
            return localDate;
        }

        public void setLocalDate(LocalDate localDate) {
            this.localDate = localDate;
        }

        public Patient getPatient() {
            return patient;
        }

        public void setPatient(Patient patient) {
            this.patient = patient;
        }

        @Override
        public String toString() {
            return "Visit{" +
                    "localDate=" + localDate +
                    "patient=" + patient +
                    '}';
        }
    }

    static class Patient {
        private String name;
        private String pesel;

        public Patient(String name, String pesel) {
            this.name = name;
            this.pesel = pesel;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPesel() {
            return pesel;
        }

        public void setPesel(String pesel) {
            this.pesel = pesel;
        }

        @Override
        public String toString() {
            return "Patient{" +
                    "name='" + name + '\'' +
                    ", pesel='" + pesel + '\'' +
                    '}';
        }
    }
}
