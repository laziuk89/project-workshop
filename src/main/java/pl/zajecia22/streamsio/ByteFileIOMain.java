package pl.zajecia22.streamsio;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class ByteFileIOMain {
    public static void main(String[] args) {

//        Scanner scanner = new Scanner(System.in); --  przykład wykorzystania strumieni
        FileInputStream inputStream = null;
        FileOutputStream outputStream = null;

        try {
//            FileInputStream inputStream; -- deklaracja zmiennej
//            inputStream = new FileInputStream("D:\"obraz.jpg"); -- przypisanie wartości do zmiennej
//            powyżej jest rozdzielenie deklaracji zmiennej od przypisania

//            poniżej jest deklaracja zmiennej z przypisaniem zmiennej w jednym miejscu
//            FileInputStream inputStream = new FileInputStream("D:\"obraz.jpg"); -- deklaracja z przypisaniem

            inputStream = new FileInputStream("D:\\obraz.jpg");
            outputStream = new FileOutputStream("D:\\obraz-kopia.jpg");

            int i;
            while ((i = inputStream.read()) != -1) {
                outputStream.write(i);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }


    }
}
