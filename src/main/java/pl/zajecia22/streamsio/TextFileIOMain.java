package pl.zajecia22.streamsio;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class TextFileIOMain {
    public static void main(String[] args) {

        try (
                FileReader reader = new FileReader("D:\\dane.txt");
                FileWriter writer = new FileWriter("D:\\dane-kopia.txt")) {
//            powyżej wykorzystanie try-with-resource
            int i;
            while((i = reader.read())!=-1){
                writer.write(i);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
