package pl.homework11;

import java.util.*;

public class Homework11 {
    public static void main(String[] args) {


        List<String> owoce = new ArrayList<>();
        owoce.add("jablko");
        owoce.add("kiwi");
        owoce.add("kiwi");
        owoce.add("pomarańcz");

        System.out.println(owoce.size());
        System.out.println(owoce.get(1));

        owoce.remove(1);
        System.out.println(owoce.size());
        System.out.println(owoce.get(1));

        owoce.add(null);
        System.out.println(owoce.size());

        List<String> rtv = new Vector<>();
        rtv.add("komputer");
        rtv.add("telefon");
        rtv.add("telefon");
        rtv.add("telewizor");

        for (int i = 0; i < rtv.size(); i++) {
            System.out.println("wypisuję sprzęt rtv " + rtv.get(i)+" jest elementem " + i);

        }
        System.out.println(rtv);

        rtv.add(null);
        System.out.println(rtv);

        Set<String> warzywa = new HashSet<>();
        warzywa.add("ogorek");
        warzywa.add("pomidor");
        warzywa.add("pomidor");
        warzywa.add("papryka");

        System.out.println(warzywa);
        warzywa.remove(1);
        System.out.println(warzywa);

        Queue<String> pory = new PriorityQueue<>();
        pory.add("jesien");
        pory.add("lato");
        pory.add("wiosna");
        pory.add("wiosna");
        pory.add("wiosna");

        System.out.println(pory);
        pory.remove("wiosna");
        System.out.println(pory);


    }
}