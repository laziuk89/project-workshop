package pl.zajecia15.bikeshop;

import java.util.ArrayList;
import java.util.List;

public class Order {

    private final String clientName;
    private final Bicycle bicycle;
    private List<Bicycle> bicycles = new ArrayList<>();

    public Order(String clientName, Bicycle bicycle) {
        this.clientName = clientName;
        this.bicycle = bicycle;
    }

    public void takeOrder(Bicycle bicycle) {
        this.bicycles.add(bicycle);

    }

    @Override
    public String toString() {
        return "Order{" +
                "bicycles=" + bicycles +
                '}';
    }

}
