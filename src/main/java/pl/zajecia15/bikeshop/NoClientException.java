package pl.zajecia15.bikeshop;

public class NoClientException extends Exception {
    /////alt+insert+override


    public NoClientException(String message) {
        super(message);
    }

    public NoClientException(String message, Throwable cause) {
        super(message, cause);
    }

}
