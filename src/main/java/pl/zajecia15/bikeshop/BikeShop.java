package pl.zajecia15.bikeshop;

import java.util.ArrayList;
import java.util.List;

public class BikeShop {

    private List<Order> orders = new ArrayList<>();

    public void createOrder(String clientName, Bicycle bicycle) throws NoClientException {

        if (clientName == null || clientName.equals("")) {

            throw new NoClientException(" nie podano danych klienta");
        }
        System.out.println("creating order");
        Order order = new Order(clientName, bicycle);
        this.orders.add(order);

    }


}
