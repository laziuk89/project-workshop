package pl.zajecia15.wyjatkiPowtorka;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class WyjatkiPowtorkaMain {
    public static void main(String[] args) {

        String number = "kot";
        Integer integer = 0;
        ///////////////////////////////// wyjątki dziedziczące po runtime exception - są unchecked
        try {
            Integer.parseInt(number);
        } catch (NumberFormatException e) {

            //////////////////////////////////////trzeba podać przynajmniej e.printStackTrace();
            System.out.println(e.getMessage());
            //   e.printStackTrace();
        }
        System.out.println("kod po parsowaniu");

        ////////////////////////////////// wyjątki dziedziczące po exception - są checked(program nie skompiluje się jeśli nie obsłużymy wyjątku)
        try {
            FileInputStream fis = new FileInputStream("dane.txt");

        } catch (FileNotFoundException e) {
            e.printStackTrace();

        }

        /////////////////////////////// wyjątki dziedziczące po exception - wyjątki możemy wyłapywać zgodnie z hierarchią dziedziczenia
        try {
            FileInputStream fis2 = new FileInputStream("obraz.jpg");

        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
