package pl.zajecia19.clinic;

import pl.zajecia19.clinic.staff.Doctor;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class ClinicClient {

    private Schedule schedule;

    public ClinicClient(Schedule schedule) {
        this.schedule = schedule;
    }
    //definicja metody: public Visit searchVisit(list<Visit> visits, LocalDateTime searchDate)
    //skladowe definicji metody
    //public - specyfikator dostępu
    //opcjonalnie słowa kluczowe static/i/lub/final
    //Visit - typ zwracany z metody(może być void, czyli nic nie zwraca)
    //searchVisit - nazwa metody
    //(...) - lista parametrów metody, może nie mieć żadnych parametrów albo dowolną ilość
    //parametry metdoy np. (list<Visit> visits, LocalDateTime searchDate)

    public Visit searchVisit(LocalDateTime searchDate) {

        if (searchDate != null) {

            System.out.printf("searching visit: %s%n", searchDate);
            for (Visit visit : schedule.getVisits()) {
                LocalDateTime visitDate = visit.getDate();

                VisitStatus status = visit.getStatus();
                if (visitDate.isEqual(searchDate)
                        && (status == VisitStatus.WOLNA || status == VisitStatus.ZAREZERWOWANA)) {

                    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd - HH:MM");
                    System.out.println("found visit: " + visit);
                    return visit;

                }
            }
        }
        System.out.println("searched visit not found");
        return null;
    }

    public void bookVisit(LocalDateTime visitDate, Patient patient) {
        System.out.printf("booking visit: %s, %s%n", visitDate, patient);
        Visit foundVisit = this.searchVisit(visitDate);

        VisitStatus status = foundVisit.getStatus();

//        LocalDateTime dateTime = LocalDateTime.of()
        if (status != VisitStatus.ZAREZERWOWANA) {


            foundVisit.setPatient(patient);
            foundVisit.setStatus(VisitStatus.ZAREZERWOWANA);
            System.out.printf("booked visit: %s, %s%n", foundVisit, patient);
        }
    }

    public void cancelVisit(LocalDateTime visitDate) {
        Visit foundVisit = this.searchVisit(visitDate);
        System.out.printf("cancelling visit %s%n", foundVisit);

        VisitStatus status = foundVisit.getStatus();

        if (status == VisitStatus.ZAREZERWOWANA) {
            foundVisit.setStatus(VisitStatus.ODWOŁANA);
            foundVisit.setPatient(null);
            System.out.printf("cancelled visit %s%n", foundVisit);
        } else {
            System.out.println("unable to cancel visit");
        }
    }

    public void changeVisit(Visit visitToCancel, LocalDateTime newVisitDate) {
        System.out.printf("changing visitToCancel: current visitToCancel: %s, new visitToCancel date: %s%n", visitToCancel, newVisitDate);

        Visit foundVisit = this.searchVisit(newVisitDate);
        if (foundVisit != null) {
            Patient patient = visitToCancel.getPatient();
            LocalDateTime cancelVisitDate = visitToCancel.getDate();

            VisitStatus status = visitToCancel.getStatus();
            if (status == VisitStatus.ZAREZERWOWANA) {
                this.cancelVisit(cancelVisitDate);
                this.bookVisit(newVisitDate, patient);
                System.out.printf("changed visitToCancel: %s, %s%n", visitToCancel, newVisitDate);
            }
        }
    }

    public void searchDoctor(Doctor searchDoctor) {

        for (Doctor doctor : schedule.getDoctors()) {
            String name = doctor.getName();
            String searchName = searchDoctor.getName();

            if (name.equalsIgnoreCase(searchName)) {
                System.out.println("Found doctor is:  " + doctor);
            }
        }
    }

    public List<Doctor> searchSpeciality(Speciality speciality) {
//TODO: uzyc obiektu schedule i getDoctors(parametr metody  - wyszukiwana speciality)

        System.out.println("Searching for the speciality");
        System.out.println("Doctors.size(): " + schedule.getDoctors().size());

        List<Doctor> foundDoctors = new ArrayList<>();
//        Doctor foundDoctor = null;


        for (Doctor doctor : schedule.getDoctors() ){
          Speciality doctorSpeciality = doctor.getSpeciality();

            System.out.println(doctorSpeciality);

            if (speciality.equals(doctorSpeciality)){
//                System.out.println("Found speciality is: " + doctor);
                foundDoctors.add(doctor);
//                foundDoctor = doctor;
//                System.out.println("found doctors: " + foundDoctors);
//                return foundDoctors;
            }
        }
        System.out.println("found doctors: " + foundDoctors);
        return foundDoctors;
//        return foundDoctor;
    }
}
