package pl.zajecia19.clinic;

import pl.zajecia19.clinic.staff.Doctor;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ClinicMain {
    public static void main(String[] args) {

        Schedule schedule = new Schedule();
        ClinicClient clinicClient = new ClinicClient(schedule);
        addDoctors(schedule);
        List<Visit> visits = ScheduleUtils.fillSchedule(schedule);
//        Patient krzysztofIbisz = new Patient("Krzysztof", "Ibisz", 19, "11234567891");
//
//        Visit visitFebruary = new Visit(schedule.getDoctors().get(0), LocalDateTime.of(2019, Month.FEBRUARY, 12, 20, 30));
//        schedule.addVisit(visitFebruary);
//
//        LocalDateTime dateDecember = LocalDateTime.of(2019, Month.DECEMBER, 4, 5, 30);
//        Visit visitDecember = new Visit(schedule.getDoctors().get(first), dateDecember);
//        schedule.addVisit(visitDecember);
//
//        clinicClient.bookVisit(dateDecember, krzysztofIbisz);
//
//        clinicClient.cancelVisit(dateDecember);
//
//        System.out.println("\n#### testing changing visit method");
//        LocalDateTime dateJanuary = LocalDateTime.of(2019, Month.JANUARY, 30, 15, 30);
//        Visit visitJanuary = new Visit(schedule.getDoctors().get(first), dateJanuary);
//        schedule.addVisit(visitJanuary);
//        clinicClient.bookVisit(dateJanuary, krzysztofIbisz);
//
//        LocalDateTime dateSecondJanuary = LocalDateTime.of(2019, Month.JANUARY, 20, 10, 0);
//        Visit visitSecondJanuary = new Visit(schedule.getDoctors().get(0), dateSecondJanuary);
//        schedule.addVisit(visitSecondJanuary);
//        clinicClient.changeVisit(visitJanuary, dateSecondJanuary);

        consoleCommands();

        Scanner scanner = new Scanner(System.in);
//        while (!"q".equals(nextLine = scanner.nextLine())) {/////////zastęuje poniższe linijki
//        while (scanner.hasNext()) {
//            String nextLine = scanner.nextLine();
//            if (nextLine.equals("q")) {
//                break;
//            }
//            System.out.println(nextLine);
//        }
        String nextLine;
        while (!"q".equalsIgnoreCase(nextLine = scanner.nextLine())) {
            System.out.println(nextLine);
//            if (nextLine.equalsIgnoreCase(""))
            System.out.println("Dostępne wizyty");
            System.out.println(schedule.getVisits());
            switch (nextLine) {
                case "s": {
//                    System.out.println("podaj datę wizyty w formacie yyyy-MM-dd");
//                    String searchDate = scanner.nextLine();
//                    System.out.println(searchDate);
//                    DateTimeFormatter dtf = DateTimeFormatter.ofPattern(DateTimeFormatter.ISO_DATE_TIME.toString());
                    LocalDateTime dateTime = consoleInput(scanner, "podaj datę wizyty w formacie yyyy-MM-dd");
                    clinicClient.searchVisit(dateTime);
                    break;
                }
                case "r": {
                    LocalDateTime dateTime = consoleInput(scanner, "podaj datę rezerwacji w formacie yyyy-MM-dd");
//                    clinicClient.bookVisit(dateTime, krzysztofIbisz);
                    break;
                }
                default: {
                    System.out.println("nieznana wartość");
                    break;
                }
            }
            consoleCommands();
        }
    }

    private static LocalDateTime consoleInput(Scanner scanner, String message) {
        System.out.println(message);
        String dateString = scanner.nextLine();
        System.out.println(dateString);
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = null;

        try {
            dateTime = LocalDateTime.parse(dateString, dtf);
        } catch (DateTimeParseException e){
            e.getMessage();
        }

        return dateTime;
    }

    private static void consoleCommands() {
        System.out.println("Aby zakończyć naciśnij 'q' ");
        System.out.println("Aby wyszukać wizytę naciśnij 's'");
        System.out.println("Aby rezerwować wizytę naciśnij 'r'");
        System.out.println("Aby odwołać wizytę naciśnij 'o'");
        System.out.println("Aby zmienić wizytę naciśnij 'z'");
    }

    private static void addDoctors(Schedule schedule){

        Doctor janNowakDoctor = new Doctor(Speciality.INTERNISTA, "Jan", "Nowak", "01234567891");
        Doctor krzysztofJudym = new Doctor(Speciality.GINEKOLOG, "Krzysztof", "Judym", "01234567892");
        Doctor stefanNiesiolowski = new Doctor(Speciality.INTERNISTA, "Stefan", "Niesiolowski", "01234567893");
        Doctor krzysztofKrawczyk = new Doctor(Speciality.PEDIATRA, "Krzysztof", "Krawczyk", "01234567894");
        Doctor piotrNowak = new Doctor(Speciality.GINEKOLOG, "Piotr", "Nowak", "01234567895");
        Doctor lordVoldemort = new Doctor(Speciality.GINEKOLOG, "Lord", "Voldemort", "01234567896");
        Doctor harryPotter = new Doctor(Speciality.INTERNISTA, "Harry", "Potter", "01234567897");
        Doctor bradPitt = new Doctor(Speciality.PEDIATRA, "Brad", "Pitt", "01234567898");

        schedule.addDoctor(janNowakDoctor);
        schedule.addDoctor(krzysztofJudym);
        schedule.addDoctor(stefanNiesiolowski);
        schedule.addDoctor(krzysztofKrawczyk);
        schedule.addDoctor(piotrNowak);
        schedule.addDoctor(lordVoldemort);
        schedule.addDoctor(harryPotter);
        schedule.addDoctor(bradPitt);

    }


}
