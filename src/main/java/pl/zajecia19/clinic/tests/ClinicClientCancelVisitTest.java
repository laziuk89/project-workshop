package pl.zajecia19.clinic.tests;

import pl.zajecia19.clinic.*;
import pl.zajecia19.clinic.staff.Doctor;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

public class ClinicClientCancelVisitTest {
    public static void main(String[] args) {
        Schedule schedule = new Schedule();

        Doctor janNowakDoctor = new Doctor(Speciality.INTERNISTA, "Jan", "Nowak", "01234567891");

        Visit visitFebruary = new Visit(janNowakDoctor, LocalDateTime.of(2019, Month.FEBRUARY, 12, 20, 30));

        LocalDateTime dateDecember = LocalDateTime.of(2019, Month.DECEMBER, 4, 5, 30);
        Visit visitDecember = new Visit(janNowakDoctor, dateDecember);

        System.out.println(visitDecember);

        schedule.addVisit(visitFebruary);
        schedule.addVisit(visitDecember);

        System.out.println("VISITS: " + schedule.getVisits());

        ClinicClient clinicClient = new ClinicClient(schedule);

        Patient krzysztofIbisz = new Patient("Krzysztof", "Ibisz", 19, "11234567891");
        clinicClient.bookVisit(dateDecember, krzysztofIbisz);

        //Visit readyToCancel = clinicClient.searchVisit(visits, dateDecember);

        clinicClient.cancelVisit(dateDecember);

    }
}
