package pl.zajecia19.clinic.tests;

import pl.zajecia19.clinic.ClinicClient;
import pl.zajecia19.clinic.Schedule;
import pl.zajecia19.clinic.Speciality;
import pl.zajecia19.clinic.staff.Doctor;

public class ClinicClientSearchDoctorTest {
    public static void main(String[] args) {
        Schedule schedule = new Schedule();
        ClinicClient clinicClient = new ClinicClient(schedule);
        Doctor janKowalskiDoctor = new Doctor(Speciality.INTERNISTA, "Jan", "Kowalski", "0123456896");
        Doctor adamNowakDoctor = new Doctor(Speciality.GINEKOLOG, "Adam", "Nowak", "0123456897");
        Doctor tomaszStefanskiDoctor = new Doctor(Speciality.PEDIATRA, "Tonmasz", "Stefański", "0123456898");


        schedule.addDoctor(janKowalskiDoctor);
        schedule.addDoctor(adamNowakDoctor);
        schedule.addDoctor(tomaszStefanskiDoctor);

        clinicClient.searchDoctor(janKowalskiDoctor);


    }
}
