package pl.zajecia19.clinic;


import pl.zajecia19.clinic.staff.Doctor;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ScheduleUtils {

//    private static Schedule schedule;


    public static final int ONE_MONTH = 24 * 2 * 31;

    public static List<Visit> fillSchedule(Schedule schedule){
        List<Visit> visits = new ArrayList<>();
        Random random = new Random();

        LocalDateTime now = LocalDateTime.now();
        LocalDateTime roundFloor =  now.truncatedTo(ChronoUnit.HOURS);

        for (int i = 0; i < ONE_MONTH; i++) {
            List<Doctor> doctors = schedule.getDoctors();
            int doctorsSize = doctors.size();
            int randomNextInt = random.nextInt(doctorsSize);
            Doctor doctor = doctors.get(randomNextInt);
            LocalDateTime plus30Minutes = roundFloor.plusMinutes(i*30);

            Visit visit = new Visit(doctor, plus30Minutes);
            schedule.addVisit(visit);
            visits.add(visit);
        }
        return visits;
    }
}
