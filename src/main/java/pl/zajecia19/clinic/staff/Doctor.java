package pl.zajecia19.clinic.staff;

import pl.zajecia19.clinic.Speciality;

public class Doctor {

    private Speciality speciality;
    private String name;
    private String surname;
    private String pesel;

    public Doctor(Speciality speciality, String name, String surname, String pesel) {
        this.speciality = speciality;
        this.name = name;
        this.surname = surname;
        this.pesel = pesel;
    }

    public Speciality getSpeciality() {
        return speciality;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPesel() {
        return pesel;
    }

    public void setSpeciality(Speciality speciality) {
        this.speciality = speciality;
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "speciality=" + speciality +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", pesel='" + pesel + '\'' +
                '}';
    }
}
