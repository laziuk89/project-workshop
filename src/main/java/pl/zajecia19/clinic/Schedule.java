package pl.zajecia19.clinic;

import pl.zajecia19.clinic.staff.Doctor;

import java.util.ArrayList;
import java.util.List;

public class Schedule {
    private List<Visit> visits = new ArrayList<>();
    private List<Doctor> doctors = new ArrayList<>();

    public void addVisit(Visit visit){
        this.visits.add(visit);
    }

    public void addDoctor(Doctor doctor){
        this.doctors.add(doctor);
    }

    public List<Visit> getVisits() {
        return visits;
    }

    public List<Doctor> getDoctors() {
        return doctors;
    }

    @Override
    public String toString() {
        return "Schedule{" +
                "visits=" + visits +
                "doctors=" + doctors +
                '}';
    }
}
