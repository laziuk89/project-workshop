package pl.zajecia19.clinic;

import pl.zajecia19.clinic.staff.Doctor;

import java.time.LocalDateTime;

public class Visit {

    private Doctor doctor;
    private Patient patient;
    private VisitStatus status=VisitStatus.WOLNA;
    private LocalDateTime date;

    public Visit(Doctor doctor, LocalDateTime date) {
        this.doctor = doctor;
        this.date = date;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public VisitStatus getStatus() {
        return status;
    }

    public void setStatus(VisitStatus status) {
        this.status = status;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Visit{" +
                "doctor=" + doctor +
                ", patient=" + patient +
                ", status=" + status +
                ", date=" + date +
                "}\n";
    }
}
