package pl.zajecia4;

public class SpecialBox extends Box {

    private int velocity;

    public SpecialBox() {
    }

    public SpecialBox(String material, double weight, int velocity) {
        super(material, weight);
        this.velocity = velocity;
    }

    @Override
    public void move(int x, int y) {
        //   System.out.printf("moving x: %d, y: %d%n", x * velocity, y * velocity);
      super.move(x * velocity, y * velocity);
    }

    public void fly(){
        System.out.println("I'm flying");
    }

    @Override
    public String toString() {
        return "SpecialBox{" +
                "velocity=" + velocity +
                "} " + super.toString();
    }
}
