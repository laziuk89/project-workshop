package pl.zajecia4;

public class Box {
    private String material;
    private double weight;

    public Box() {

    }

    public Box(String material, double weight) {

        this.material = material;
        this.weight = weight;
    }

    public void move(int x, int y) {
        System.out.printf("moving x: %d, y: %d%n", x, y);
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Box{" +
                "material='" + material + '\'' +
                ", weight=" + weight +
                '}';
    }
}
