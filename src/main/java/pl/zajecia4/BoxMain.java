package pl.zajecia4;

public class BoxMain {
    public static void main(String[] args) {


        Box woodenBox = new Box("wood", 10);
        woodenBox.move(23, 55);

        //SpecialBox specialBox = new SpecialBox("plastic", first, 4);
        Box specialBox = new SpecialBox("plastic", 1, 4);
        specialBox.move(10, 10);

        System.out.println(woodenBox);
        System.out.println(specialBox);

        SpecialBox flyingBox = new SpecialBox();
        flyingBox.fly();

//        woodenBox.fly();
//        specialBox.fly();

    }
}
