package pl.zajecia4.instructions;

public class PostOfficeMain {
    public static void main(String[] args) {

        Postman janPostman = new Postman("Jan", 56);
        janPostman.setExperience(15);
        PostOffice marszalkowskaPostOffice = new PostOffice();

        marszalkowskaPostOffice.redirectPostman(janPostman);
        System.out.println();
        Postman januszPostman = new Postman("Janusz", 30);
        januszPostman.setExperience(2);
        marszalkowskaPostOffice.redirectPostman(januszPostman);

        //in line variable
        //marszalkowskaPostOffice.redirectPostman(new Postman("Arek", 19));
        marszalkowskaPostOffice.text(janPostman.getExperience());

        PostOffice.showPostman(janPostman);
    }

}
