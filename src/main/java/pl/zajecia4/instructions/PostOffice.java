package pl.zajecia4.instructions;

public class PostOffice {
    public static void showPostman(Postman postman){
        System.out.println("prezentuje listonosza " + postman);
    }
    public void redirectPostman(Postman listonosz) {
//        int age;///deklaracja zmiennej
//        age = listonosz.getAge();// przypisanie wartości do zmiennej; przypisanie wartości zwracanej z metody

        int age = listonosz.getAge(); //deklaracja zmiennej z przypisaniem
        System.out.println("listonosz ma lat: " + age);

        System.out.println("przekierwouje listonosza " + listonosz);

        int experiecne = listonosz.getExperience();
        System.out.printf("exp listonosza: %d%n", listonosz.getExperience());

        if (age > 45) {
            System.out.println("wiekowy listonoz");
        } else {
            System.out.println("młody listonosz");
        }

        if (listonosz.getExperience() >= 7) {
            System.out.println("przyzwoity staż");

        }

    }

    public void text(int exp) {
        System.out.println("listonosz dobrze zarabia");
        switch (exp) {
            case 2: {
                System.out.println("2 lata doświadczenia");
                break;
            }
            case 15: {
                System.out.println("15 lat doświadczenia");
                break;
            }
            default: {
                System.out.println("nierozpoznane doświadczenie");
            }
        }

    }

}
