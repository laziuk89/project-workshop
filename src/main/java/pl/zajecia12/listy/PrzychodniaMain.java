package pl.zajecia12.listy;

import java.util.ArrayList;
import java.util.List;

public class PrzychodniaMain {
    public static void main(String[] args) {
        List<Termin> terminy = new ArrayList<>();

        Lekarz janLekarz = new Lekarz("Jan", "Kowalski", Specjalizacja.GINEKOLOG);
        Termin naDzis = new Termin("2019-01-09", janLekarz);
//        System.out.println(janLekarz);
//        System.out.println(naDzis);

        Lekarz nowakLekarz = new Lekarz("Stefan", "Nowak", Specjalizacja.INTERNISTA);
        Termin naJutro = new Termin("2019-01-10", nowakLekarz);
//        System.out.println(nowakLekarz);
//        System.out.println(naJutro);

        Lekarz tomaszLekarz = new Lekarz("Tomasz", "Rafalski", Specjalizacja.PEDIATRA);
        Termin naZaTydzien = new Termin("2019-01-16", tomaszLekarz);

        terminy.add(naDzis);
        terminy.add(naJutro);
        terminy.add(naZaTydzien);

        //   System.out.println(terminy);
        for (Termin termin : terminy) {
            System.out.println("dostępne terminy w przychodni " + termin);
            if (termin != null) {
                Lekarz lekarz = termin.getLekarz();
                if (lekarz.getSpecjalizacja()==Specjalizacja.GINEKOLOG){
                    System.out.printf("w terminie %s jest dostępny ginekolog o imieniu %s%n",
                            termin.getDate(), lekarz.getName());
                }

            }
        }

    }
}
