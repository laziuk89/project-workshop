package pl.zajecia12.listy;

public class Lekarz {
    private String name;
    private String lastName;
    private Specjalizacja specjalizacja;

    public Lekarz(String name, String lastName, Specjalizacja specjalizacja) {
        this.name = name;
        this.lastName = lastName;
        this.specjalizacja = specjalizacja;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Specjalizacja getSpecjalizacja() {
        return specjalizacja;
    }

    public void setSpecjalizacja(Specjalizacja specjalizacja) {
        this.specjalizacja = specjalizacja;
    }


    @Override
    public String toString() {
        return "Lekarz{" +
                "name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", specjalizacja=" + specjalizacja +
                '}';
    }
}
