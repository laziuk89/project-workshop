package pl.zajecia12.listy;

public class Termin {
    private String date;
    private Lekarz lekarz;

    public Termin(String date, Lekarz lekarz) {
        this.date = date;
        this.lekarz = lekarz;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Lekarz getLekarz() {
        return lekarz;
    }

    public void setLekarz(Lekarz lekarz) {
        this.lekarz = lekarz;
    }

    @Override
    public String toString() {
        return "Termin{" +
                "date='" + date + '\'' +
                ", lekarz=" + lekarz +
                '}';
    }
}
