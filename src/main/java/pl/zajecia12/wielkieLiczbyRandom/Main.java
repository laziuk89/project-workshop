package pl.zajecia12.wielkieLiczbyRandom;

import java.math.BigDecimal;

public class Main {
    public static void main(String[] args) {

        int suma, skladnik1, skladnik2;
        suma=0;
        skladnik1=1;
        skladnik2=20;

        suma=skladnik1+skladnik2;
        System.out.println(suma);

        double dSuma = 0.0;
        double dSkladniki1 = 1.75;
        double dSkladniki2 = 3;

        dSuma=dSkladniki1+dSkladniki2;
        System.out.println(dSuma);

        BigDecimal zero = BigDecimal.ZERO;
        BigDecimal bdSkladnik1 = new BigDecimal(2.55);
        BigDecimal bdSkladnik2 = new BigDecimal(34.78);
        BigDecimal add = zero.add(bdSkladnik1);

        System.out.println(zero);
        System.out.println(add);
        System.out.println(zero);


    }
}
