package pl.zajecia21.generics;
///<T> - type parameter (typ parametru)
///<?> - unknown type
///<Object> - type argument (typ argumentu)
///list<Object> objects - parametrized type (typ sparametryzowany)
public class Box<T> {
    private T t;

    public Box(T t) {
        this.t = t;
    }

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }

    @Override
    public String toString() {
        return "Box{" +
                "t=" + t +
                '}';
    }
}
