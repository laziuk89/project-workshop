package pl.zajecia21.generics;

public interface Pair<K,V> {
    K getKey();
    V getValue();

}
