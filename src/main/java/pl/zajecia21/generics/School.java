package pl.zajecia21.generics;

public class School<T> {

    private T t;
    private String address;

    public School() {
    }

    public School(T t, String address) {
        this.t = t;
        this.address = address;
    }

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "School{" +
                "t=" + t +
                ", address='" + address + '\'' +
                '}';
    }
}
