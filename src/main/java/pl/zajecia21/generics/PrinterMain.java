package pl.zajecia21.generics;

public class PrinterMain {
    public static void main(String[] args) {
        Printer printer = new Printer();
        Printer<String> printerString = new Printer<>("Ala");

        Printer<Integer> printerInteger = new Printer<>(1);


    }
}
