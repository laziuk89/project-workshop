package pl.zajecia21.generics;

public class ComplexPair implements Pair<String, Integer>{

    private String key;
    private Integer value;

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public Integer getValue() {
        return value;
    }
}
