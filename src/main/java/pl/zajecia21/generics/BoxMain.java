package pl.zajecia21.generics;

public class BoxMain {
    public static void main(String[] args) {
        Box<String> boxOfStrings = new Box<>("new Box of Strings");
        System.out.println(boxOfStrings);

        Box<Integer> boxOfIntegers = new Box<>(1);
        System.out.println(boxOfIntegers);

        String string = boxOfStrings.getT();
        Integer integer = boxOfIntegers.getT();

        GenericsUtils.unbox(boxOfStrings);
    }
}
