package pl.zajecia21.generics.pizza;

public class Pizza<T> {

    private T type;

    public Pizza(T type) {
        this.type = type;
    }
}
