package pl.zajecia21.generics.pizza;

public class PizzaMain {
    public static void main(String[] args) {

        Pizza<Vegan> pizzaVegan = new Pizza<>(new Vegan());
        Pizza<Meat> pizzaMeat = new Pizza<>(new Meat());

    }
}
