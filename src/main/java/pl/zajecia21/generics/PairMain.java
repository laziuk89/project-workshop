package pl.zajecia21.generics;

public class PairMain {
    public static void main(String[] args) {
        SimplePair<String, Integer> simplePair = new SimplePair<>();

        simplePair.setKey("setter key");
        simplePair.setValue(333);

        String string = simplePair.getKey();
        Integer integer = simplePair.getValue();
        System.out.println(string + " " + integer);
    }
}
