package pl.zajecia21.homework;

public class Main {
    public static void main(String[] args) {
        Restaurant<String, Integer> restaurant = new Restaurant<>("swietojanska", 4);
        restaurant.setType("inna");
        String type = restaurant.getType();

        System.out.println(restaurant);
        System.out.println(type);
    }
}
