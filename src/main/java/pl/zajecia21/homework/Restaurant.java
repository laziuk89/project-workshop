package pl.zajecia21.homework;

public class Restaurant<T, V> {

    private T type;
    private V val;

    public Restaurant(T type, V val) {
        this.type = type;
        this.val = val;
    }

    public T getType() {
        return type;
    }

    public void setType(T type) {
        this.type = type;
    }

    public V getVal() {
        return val;
    }

    public void setVal(V val) {
        this.val = val;
    }
}
