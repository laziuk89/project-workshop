package pl.zajecia13.wyjatki;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.DateFormat;
import java.util.Date;

public class WyjatkiMain {
    public static void main(String[] args) {
        Integer suma = new Integer(1);
        try {
            Integer.parseInt("kod"); // wyjątek nieobsługiwany
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }finally{
            System.out.println("pierwszy błąd");
        }

        String string = null;
        // string.length();

        System.out.println("będę czytał z pliku");
        try {
            System.out.println("będę odczytywał dane z pliku");
            FileInputStream fis = new FileInputStream("start.txt");
            System.out.println("odczytuję dane z pliku");
        } catch (FileNotFoundException e) {
            System.out.println("cos poszlo nie tak");
            //  e.printStackTrace();
        } finally {
            System.out.println("ostatecznie trzeba cos z tym zrobic");
        }

        System.out.println("odczytałem dane z pliku w systemoucie");

        //DateFormat.
    }
}
