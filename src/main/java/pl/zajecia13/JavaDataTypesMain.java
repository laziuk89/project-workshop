package pl.zajecia13;

public class JavaDataTypesMain {
    public static void main(String[] args) {

        // po lewej typy proste || po prawej typy złożone
        // first bajt = 8 bitów
        byte b; Byte bb;// 8 bitów
        short s; Short ss;// 16 bitów
        int i; Integer ii;// 32 bitów
        long l; Long ll;// 64 bitów
        float f; Float ff;// 32 bitów
        double d; Double dd;// 64 bitów
        boolean boo; Boolean boo2;//first bit
        char c; Character cc;// 16 bitów

        b=1;
        //bb=256;
        bb=1;
        bb=null;
      //  b=null;
        boo = true; boo = false; //boo = null;
        boo2 = true; boo2 = false; boo2 = null;
        String string = null;

        pl.zajecia13.MyInteger myInteger = new MyInteger();
        MyInteger mi1 = new MyInteger();
        MyInteger mi2 = new pl.zajecia13.MyInteger();
        pl.zajecia13.MyInteger mi3 = new pl.zajecia13.MyInteger();

        pl.zajecia13.types.MyInteger my1 = new pl.zajecia13.types.MyInteger();

    }
}
