package pl.zajecia27.sockets;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class EchoServerSocket {
    public static void main(String[] args) {

        try {
            ServerSocket serverSocket = new ServerSocket(6666);
            Socket clientSocket = serverSocket.accept();
            InputStream inputStream = clientSocket.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String readLine = bufferedReader.readLine();
            System.out.println("server receiving from client: " + readLine);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
