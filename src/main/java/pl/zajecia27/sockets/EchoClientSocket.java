package pl.zajecia27.sockets;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

public class EchoClientSocket {
    public static void main(String[] args) {
        try {
            Socket socket = new Socket("localhost", 6666);
            OutputStream outputStream = socket.getOutputStream();
            PrintWriter printWriter = new PrintWriter(outputStream, true);
            printWriter.println("hello world, sending message through socket");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
