package pl.homework3.nowyProjekt;

public class FootballerMain {
    public static void main(String[] args) {

        Footballer ronaldo = new Footballer("Cristiano", 33, "juventus", false);
        System.out.println(ronaldo);

        Footballer rooney = new Footballer("Wayne", 35, "LA Galaxy", true);
        System.out.println(rooney);

        Footballer messi = new Footballer("Leo", 30, "barcelona", false);
        System.out.println(messi);

        System.out.println();
        new Footballer(22);

        Footballer hajto = new Footballer(34);
        System.out.println(hajto);
        System.out.println();
        Footballer footballer = new Footballer("Maciej", 33, "huragan", true);
        System.out.println(footballer);

        Footballer.Forward forward = footballer.new Forward(4000);
        System.out.println(forward);



    }

}
