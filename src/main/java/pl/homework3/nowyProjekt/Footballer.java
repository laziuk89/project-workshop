package pl.homework3.nowyProjekt;

public class Footballer {

    private String name;
    private int age;
    private String club;
    private boolean teamPlayer;
    private int caps;


    public Footballer(int caps) {

        this.caps = caps;
        //   System.out.println("simple constructor with one variable running");

    }

    public Footballer(String name, int age, String club, boolean teamPlayer) {

        this.name = name;
        this.age = age;
        this.club = club;
        System.out.println(name + " " + age + " " + club);

    }

    public void speak() {

        Forward forward = new Forward(500);
    }

    public int getCaps() {
        return caps;
    }

    public void setCaps(int caps) {
        this.caps = caps;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getClub() {
        return club;
    }

    public void setClub(String club) {
        this.club = club;
    }

    public boolean isTeamPlayer() {
        return teamPlayer;
    }

    public void setTeamPlayer(boolean teamPlayer) {
        this.teamPlayer = teamPlayer;
    }

    @Override
    public String toString() {
        return "Footballer{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", club='" + club + '\'' +
                ", teamPlayer='" + teamPlayer + '\'' +
                ", caps='" + caps + '\'' +
                '}';
    }

    class Forward {
        private int wage;

        public Forward(int wage) {

            this.wage = wage;
        }
            public void scream() {

                System.out.println(name);

        }

        public int getWage() {
            return wage;
        }

        public void setWage(int wage) {
            this.wage = wage;
        }

        @Override
        public String toString() {
            return "Forward{" +
                    "wage=" + wage +
                    '}';
        }
    }

}
