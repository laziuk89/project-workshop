package pl.homework3;

public class MachineMain {
    public static void main(String[] args) {

        Machine car = new Machine("jakis tekst");
        System.out.println(Machine.NUMBER);

        Machine machine1 = new Machine("drugi tekst");
        System.out.println(machine1);
        System.out.println(car);

        Machine.Robot android = new Machine.Robot("Poland");
        System.out.println(android);
        Plane boeing = new Plane(88);

        Machine anonymousMachine = new Machine() {

            private int length;

            public void command(int length) {
                System.out.println("your command + length");

            }

            @Override
            public String toString() {
                return "$classname{" +
                        "length=" + length +
                        "} " + super.toString();
            }
        };

    }
}
