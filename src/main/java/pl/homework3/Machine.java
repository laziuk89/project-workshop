package pl.homework3;

public class Machine {
    public static final int NUMBER = 10;
    private String name;

    public Machine() {

    }

    public Machine(String text) {
        this.name = text;
    }

    public void speak() {

        Auto auto1 = new Auto("ferrari");

    }

    @Override
    public String toString() {
        return "Machine{" +
                "name='" + name + '\'' +
                '}';
    }

    class Auto extends Machine {
        private String brand;

        public Auto(String brand) {
            this.brand = brand;
        }

        public void speak() {
            System.out.println(name);
        }
    }

    static class Robot {
        private String countryOfOrigin;

        public Robot(String countryOfOrigin) {
            this.countryOfOrigin = countryOfOrigin;
        }

        public void signal() {
            System.out.println("alarm");
        }

        @Override
        public String toString() {
            return "Robot{" +
                    "countryOfOrigin='" + countryOfOrigin + '\'' +
                    '}';
        }
    }

}

/////tutaj bez zadnego modifier????? np.  private public
class Plane {

    public int quantity;

    public Plane(int quantity) {
        this.quantity = quantity;
    }

    public void message() {
        System.out.println("planes are great");

    }
}