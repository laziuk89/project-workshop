package pl.homework3.innerClass;

public class Computer {

    private String name;

        class Asus {

            private int width;

            public Asus(int width){
                this.width=width;
            }
            public void speak(){
                System.out.println(name);
            }

            public int getWidth() {
                return width;
            }

            public void setWidth(int width) {
                this.width = width;
            }

            @Override
            public String toString() {
                return "Asus{" +
                        "width=" + width +
                        '}';
            }
        }

}

