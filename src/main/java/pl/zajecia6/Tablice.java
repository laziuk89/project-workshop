package pl.zajecia6;

import java.util.Arrays;

public class Tablice {
    public static void main(String[] args) {

        String[] imiona= new String[3];
        imiona[0]="Maciej";
        //imiona[3]="jacek";


        System.out.println(Arrays.asList(imiona));

        System.out.println(imiona[0]);
        Student[] studenci = new Student[5];
        studenci[0]=new Student("adam", 18);
        studenci[4]=new Student("jerzy", 76);
        System.out.println(Arrays.asList(studenci));

    }
}
