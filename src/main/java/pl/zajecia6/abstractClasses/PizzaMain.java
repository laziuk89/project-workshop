package pl.zajecia6.abstractClasses;

public class PizzaMain {
    public static void main(String[] args) {

        Pizza pizza1 = new Pizza("hawasjka", 30);
        System.out.println(pizza1);
        pizza1.describePizza();
        System.out.println(pizza1);

        VeganPizza veganPizza = new VeganPizza("cukinia", 40, true);
        System.out.println(veganPizza);

       //veganPizza.describePizza();

        veganPizza.show();
        veganPizza.describePizza();

        Pizza pizza2 = new Pizza();
        //pizza2.show();
        pizza2.describePizza();

    }
}
