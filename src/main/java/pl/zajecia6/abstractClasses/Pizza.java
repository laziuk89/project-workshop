package pl.zajecia6.abstractClasses;

public class Pizza {

    private String name;
    private int size;

    public Pizza(){

    }

    public Pizza(String name, int size){

        this.name=name;
        this.size=size;

    }

    public void describePizza(){
      //  increaseSize(2);
        System.out.printf("pizza name %s, size %d%n", name, size);
    }

    private void increaseSize(int inc){

        //this.size = this.size +inc;
        this.size += inc;

    }

    @Override
    public String toString() {
        return "Pizza{" +
                "name='" + name + '\'' +
                ", size=" + size +
                '}';
    }
}
