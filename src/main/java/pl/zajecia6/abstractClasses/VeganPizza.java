package pl.zajecia6.abstractClasses;

public class VeganPizza extends Pizza {


    private boolean vegan;

    public VeganPizza(){

    }

    public VeganPizza(String name, int size, boolean vegan){
        super(name, size);
        this.vegan=vegan;
    }

    public void show(){
        System.out.println("showing vegan pizza");
    }

    @Override
    public void describePizza(){
        super.describePizza();
        System.out.println("describe Pizza from Vegan Pizza");
    }
    @Override
    public String toString() {
        return "VeganPizza{" +
                "vegan=" + vegan +
                "} " + super.toString();
    }
}
