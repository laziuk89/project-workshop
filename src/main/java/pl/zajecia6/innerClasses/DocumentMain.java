package pl.zajecia6.innerClasses;

public class DocumentMain {
    public static void main(String[] args) {

        //Document htmlDocument = new Document();
        Document htmlDocument = new HtmlDocument();

        //klasa anonimowa
        Document pdfDocument = new Document() {
            @Override
            public void read() {
                System.out.println("reading PDF document");
            }
        };
        htmlDocument.read();
        pdfDocument.read();

        OrphanDocument orphanDocument1 = new OrphanDocument();
        orphanDocument1.change(3);

        //ctrl+spacja - podpowiada składnię
        //pl.laziuk.maciej.zajecia.java.zajecia6.innerClasses.docs.Document docs = new pl.laziuk.maciej.zajecia.java.zajecia6.innerClasses.docs.Document();
    }
}

