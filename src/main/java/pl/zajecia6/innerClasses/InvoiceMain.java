package pl.zajecia6.innerClasses;

public class InvoiceMain {
    public static void main(String[] args) {

        Invoice laptopInvoice = new Invoice();

        Invoice.Contractor contractor1 = laptopInvoice.new Contractor();
        contractor1.changeInvoiceDate();

        Invoice.Item item1 = new Invoice.Item();

        Invoice innerInvoice = new Invoice(){

        };
    }

}
