package pl.zajecia6.innerClasses;

public class HtmlDocument extends Document {

    //implementuje/nadpisuje metodę abstrakcyjną w klasie abstrakcyjnej (można napisać albo czerwona żarówka)
    @Override
    public void read() {
        System.out.println("reading HMTL document");
    }
}
