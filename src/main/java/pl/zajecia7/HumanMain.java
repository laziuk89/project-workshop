package pl.zajecia7;

public class HumanMain {
    public static void main(String[] args) {

        Human human1 = new Human(18, 170, "brown", Gender.GENDER_MALE);

        Gender gender1 = human1.getGender();
      //  System.out.println("płeć " + gender1.getName());
        decodeGender(gender1);

        System.out.println(human1.toString());

        Human human2 = new Human(15, 155, "ginger", Gender.GENDER_FEMALE);
        Gender gender2 = human2.getGender();
      //  System.out.println("płeć " + gender2.getName());
        decodeGender(gender2);
        System.out.println(human2);



    }
    public static void decodeGender(Gender gender){
        switch (gender){
            case GENDER_FEMALE:{
                System.out.println("zdekodowana płeć: " + gender.getName());
                break;
            }
            case GENDER_MALE:{
                System.out.println("zdekodowana płeć " + gender.getName());
                break;
            }
            default:{
                System.out.println("zdekodowana płeć nieokreślona");
            }
        }
    }
}