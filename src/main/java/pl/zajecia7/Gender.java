package pl.zajecia7;

public enum Gender {
    GENDER_MALE("mężczyzna"), GENDER_FEMALE("kobieta");

    private String name;

    Gender() {

    }

    Gender(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


}
