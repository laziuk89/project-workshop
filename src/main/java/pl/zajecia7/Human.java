package pl.zajecia7;

public class Human {
    public static final int MALE = 0;
    public static final int FEMALE = 1;

    private int age;
    private int height;
    private String hairColour;
//    private boolean male;
//    private int sex;
    private Gender gender;


//    public Human(int age, int height, String hairColour, boolean male, int sex) {
    public Human(int age, int height, String hairColour, Gender gender) {
        this.age = age;
        this.height = height;
        this.hairColour = hairColour;
//        this.male = male;
//        this.sex = sex;
        this.gender=gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getHairColour() {
        return hairColour;
    }

    public void setHairColour(String hairColour) {
        this.hairColour = hairColour;
    }

//    public boolean isMale() {
//        return male;
//    }
//
//    public void setMale(boolean male) {
//        this.male = male;
//    }
//
//    public int getSex() {
//        return sex;
//    }
//
//    public void setSex(int sex) {
//        this.sex = sex;
//    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "Human{" +
                "age=" + age +
                ", height=" + height +
                ", hairColour='" + hairColour + '\'' +
//                ", male=" + male +
//                ", sex=" + sex +
                ", gender=" + gender +
                '}';
    }
}
